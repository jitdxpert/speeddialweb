<?php

error_reporting(0);
ini_set('display_errors', 'Off');

include(dirname(__FILE__).'/config.php');

$json = inputs();
$case = $json->case;
DB::table('logs')->insert(array(
  'content' => json_encode($json),
  'uri' => $case,
  'timestamp' => date('Y-m-d H:i:s'),
));

switch($case){
  case 'LoginProcess':
  	LoginProcess();
  	break;
  case 'GetAdminDetails':
  	GetUserDetails();
  	break;

  case 'GetAllUsers':
  	GetAllUsers();
  	break;

  case 'SavePageContent':
  	SavePageContent();
  	break;
  case 'GetPageContent':
  	GetPageContent();
  	break;

  case 'UploadVideo':
  	UploadVideo();
  	break;
  case 'GetAllVideos':
  	GetAllVideos();
  	break;
  case 'GetVideoByID':
  	GetVideoByID();
  	break;
  case 'TrashVideo':
  	TrashVideo();
  	break;

  case 'UploadAudio':
  	UploadAudio();
  	break;
  case 'GetAllAudios':
  	GetAllAudios();
  	break;
  case 'GetAudioByID':
  	GetAudioByID();
  	break;
  case 'TrashAudio':
  	TrashAudio();
  	break;

  case 'CreateEditProduct':
  	CreateEditProduct();
  	break;
  case 'GetAllProducts':
  	GetAllProducts();
  	break;
  case 'TrashProduct':
  	TrashProduct();
  	break;
  case 'GetProductByID':
  	GetProductByID();
  	break;

  case 'CreateEditEvent':
  	CreateEditEvent();
  	break;
  case 'GetAllEvents':
  	GetAllEvents();
  	break;
  case 'TrashEvent':
  	TrashEvent();
  	break;
  case 'GetEventByID':
  	GetEventByID();
  	break;

  case 'GetAllSupports':
  	GetAllSupports();
  	break;
  case 'TrashSupport':
  	TrashSupport();
  	break;
  case 'GetSupportByID':
  	GetSupportByID();
  	break;
  case 'ReplySupport':
  	ReplySupport();
  	break;

  case 'GetAllConsultations':
  	GetAllConsultations();
  	break;
  case 'TrashConsult':
  	TrashConsult();
  	break;
  case 'GetConsultByID':
  	GetConsultByID();
  	break;
  case 'ReplyConsult':
  	ReplyConsult();
  	break;

  case 'ScheduleNotification':
    ScheduleNotification();
    break;

  default:
  	echo '404! Page Not Found';
  	break;
}

// Admin Related Functions
function LoginProcess(){
  $res = array();

  $json = inputs();
  if(isset($json->username) && isset($json->password)){
  	$admin = DB::table('admin')->where('Admin_Email', '=', $json->username)->where('Admin_Password', '=', $json->password)->first();
  	if(count($admin) > 0){
  		session_start();
  		$_SESSION['Admin_ID'] = $admin['Admin_ID'];
  		$res['code'] = 0;
  		$res['text'] = $admin['Admin_ID'];
  	}else{
  		$res['code'] = 1;
  		$res['text'] = 'Entered wrong Email or Password.';
  	}
  }else{
  	$res['code'] = 2;
  	$res['text'] = 'Email & Password are required.';
  }

  echo json_encode($res);
}

function GetAdminDetails(){
  $res = array();

  $json = inputs();
  $Admin_ID = $json->id;
  if($Admin_ID){
  	$admin = DB::table('admin')->where('Admin_ID', '=', $Admin_ID)->first();
  	if(count($admin) == 1){
  		$res['code'] = 0;
  		$res['text'] = $admin;
  	}else{
  		$res['code'] = 1;
  		$res['text'] = 'Your session expire, please login again.';
  	}
  }else{
  	$res['code'] = 2;
  	$res['text'] = 'Your session expire, please login again.';
  }

  echo json_encode($res);
}

// User Related Functions
function GetAllUsers(){
  $res = array();

  $json = inputs();

  $avgLoginSpan = array();
  $currentDate = date('Y-m-d H:i:s');
  $oneMonthBeforeNow = date('Y-m-d H:i:s', strtotime("-30 days", strtotime($currentDate)));

  $filter = isset($json->filter) ? $json->filter : '';
  $type = isset($json->type) ? $json->type : '';
  if($type == 'device'){
    $users = DB::table('users')->where('User_Device', '=', $filter)->orderBy('User_ID', 'desc')->get();
  }else{
    $users = DB::table('users')->orderBy('User_ID', 'desc')->get();
  }
  if(count($users) > 0){
  	$i = 0;
  	$res['UsersCount'] = count($users);
  	foreach($users as $user){
  		if(!empty($user['User_Email'])){
  			$loginSpan = array();
  			$logTimes = DB::table('logins')->where('Login_Time', '!=', '0000-00-00 00:00:00')->where('Logout_Time', '!=', '0000-00-00 00:00:00')->where('User_ID', '=', $user['User_ID'])->where('Login_Time', '>=', $oneMonthBeforeNow)->where('Login_Time', '<=', $currentDate)->get();
  			foreach($logTimes as $logTime){
  				$loginSpan[] = strtotime($logTime['Logout_Time']) - strtotime($logTime['Login_Time']);
  			}
  			if(count($loginSpan) > 0){
  				$avgLogTime = number_format(array_sum($loginSpan)/30, 2);
  			}else{
			    $avgLogTime = 0;
  			}

		    $res['text'][$i] = $user;
		    $res['text'][$i]['AvgLogTime'] = $avgLogTime;

  			$i++;
  		}
  	}
  	$res['code'] = 0;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No users are available.';
  }

  $oneWeekBeforeNow = date('Y-m-d H:i:s', strtotime("-7 days", strtotime($currentDate)));
  $Last7DaysRegister = DB::table('users')->where('User_RegisterOn', '>=', $oneWeekBeforeNow)->where('User_RegisterOn', '<=', $currentDate)->get();
  $res['UsersLast7Days'] = count($Last7DaysRegister);

  $AvgTimeSession = DB::table('logins')->where('Login_Time', '!=', '0000-00-00 00:00:00')->where('Logout_Time', '!=', '0000-00-00 00:00:00')->get();
  foreach($AvgTimeSession as $session){
  	$avgLoginSpan[] = strtotime($session['Logout_Time']) - strtotime($session['Login_Time']);
  }
  $res['AvgSessionTime'] = number_format(array_sum($avgLoginSpan)/count($AvgTimeSession), 2);

  $MonthlyActiveUsers = DB::table('logins')->where('Login_Time', '!=', '0000-00-00 00:00:00')->where('Logout_Time', '!=', '0000-00-00 00:00:00')->where('Login_Time', '>=', $oneMonthBeforeNow)->where('Login_Time', '<=', $currentDate)->groupBy('User_ID')->count();

  $res['MonthlyActiveUsers'] = $MonthlyActiveUsers;

  echo json_encode($res);
}

function SavePageContent(){
  $res = array();

  $json = inputs();

  if(!empty($json->content)){
  	$page = $json->page;
  	$content = $json->content;

  	$pageContent = DB::table('contents')->where('Page_Name', '=', $page)->first();
  	$pageArray = array(
  		'Page_Content' => $content,
  		'UpdatedOn' => date('Y-m-d H:i:s')
  	);
  	if(count($pageContent) == 0){
  		$pageArray['Page_Name'] = $page;
  		DB::table('contents')->insert($pageArray);
  	}else{
  		DB::table('contents')->where('Page_Name', '=', $page)->update($pageArray);
  	}
  	$words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $page);
  	$res['code'] = 0;
  	$res['text'] = $words.' has been successfully saved.';
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'Please enter page content before save.';
  }

  echo json_encode($res);
}

function GetPageContent(){
  $res = array();

  $json = inputs();

  if(!empty($json->page)){
  	$page = $json->page;
  	$pageContent = DB::table('contents')->where('Page_Name', '=', $page)->first();
  	if(count($pageContent) > 0){
  		$res['code'] = 0;
  		$res['text'] = $pageContent;
  	}
  }

  echo json_encode($res);
}

// Video Related Functions
function UploadVideo(){
  $res = array();

  $json = inputs();

  if(isset($json)){
  	$allowed = array('mp4', 'MP4');
  	$videoTitle = $json->title;
  	$videoDesc = $json->desc;
  	if(!isset($json->id)){
  		$videoUpld = date('Y-m-d H:i:s');
  		if(!empty($_FILES)){
  			$videoTmp = $_FILES['file']['tmp_name'];
  			$videoName = $_FILES['file']['name'];
  			$videoNew = time().'-'.$videoName;
  			$videoPath = BASE_DIR.'/uploads/videos/'.$videoNew;
  			$ext = pathinfo($videoName, PATHINFO_EXTENSION);
  			if(in_array($ext, $allowed)){
  				if(move_uploaded_file($videoTmp, $videoPath)){
  					$videoArray = array(
  						'Video_Title' => $videoTitle,
  						'Video_Original' => $videoName,
  						'Video_File' => $videoNew,
  						'Video_Desc' => $videoDesc,
  						'Video_UploadOn' => $videoUpld
  					);
  					DB::table('videos')->insert($videoArray);
  					$videos = DB::table('videos')->orderBy('Video_ID', 'desc')->get();
  					if(count($videos) > 0){
  						$res['code'] = 0;
  						$res['text'] = $videos;
  					}else{
  						$res['code'] = 1;
  						$res['text'] = 'No videos are available.';
  					}
  				}
  			}else{
  				$res['code'] = 2;
  				$res['text'] = 'Video format is not allowed.';
  			}
  		}
  	}else{
  		$videoId = $json->id;
  		$video = DB::table('videos')->where('Video_ID', '=', $videoId)->first();
  		if(!empty($_FILES)){
  			$videoTmp = $_FILES['file']['tmp_name'];
  			$videoName = $_FILES['file']['name'];
  			$videoNew = time().'-'.$videoName;
  			$videoPath = BASE_DIR.'/uploads/videos/'.$videoNew;
  			$ext = pathinfo($videoName, PATHINFO_EXTENSION);
  			if(in_array($ext, $allowed)){
  				if(move_uploaded_file($videoTmp, $videoPath)){
  					$videoArray = array(
  						'Video_Title' => $videoTitle,
  						'Video_Original' => $videoName,
  						'Video_File' => $videoNew,
  						'Video_Desc' => $videoDesc
  					);
  					DB::table('videos')->where('Video_ID', '=', $videoId)->update($videoArray);
  					unlink(BASE_DIR.'/uploads/videos/'.$video['Video_File']);
  					$videos = DB::table('videos')->orderBy('Video_ID', 'desc')->get();
  					if(count($videos) > 0){
  						$res['code'] = 0;
  						$res['text'] = $videos;
  					}else{
  						$res['code'] = 1;
  						$res['text'] = 'No videos are available.';
  					}
  				}
  			}else{
  				$res['code'] = 2;
  				$res['text'] = 'Video format is not allowed.';
  			}
  		}else{
  			$videoArray = array(
  				'Video_Title' => $videoTitle,
  				'Video_Desc' => $videoDesc
  			);
  			DB::table('videos')->where('Video_ID', '=', $videoId)->update($videoArray);
  			$videos = DB::table('videos')->orderBy('Video_ID', 'desc')->get();
  			if(count($videos) > 0){
  				$res['code'] = 0;
  				$res['text'] = $videos;
  			}else{
  				$res['code'] = 1;
  				$res['text'] = 'No videos are available.';
  			}
  		}
  	}
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'Fields are required.';
  }

  echo json_encode($res);
}

function GetAllVideos(){
  $res = array();

  $videos = DB::table('videos')->orderBy('Video_ID', 'desc')->get();
  if(count($videos) > 0){
  	$res['code'] = 0;
  	$res['text'] = $videos;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No videos are available.';
  }

  echo json_encode($res);
}

function GetVideoByID(){
  $res = array();

  $json = inputs();

  $videoId = $json->VideoID;
  $video = DB::table('videos')->where('Video_ID', '=', $videoId)->first();
  if(count($video) > 0){
  	$res['code'] = 0;
  	$res['text'] = $video;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No video is available.';
  }

  echo json_encode($res);
}

function TrashVideo(){
  $res = array();

  $json = inputs();

  if(!empty($json->videoId)){
  	$videoId = $json->videoId;
  	$video = DB::table('videos')->where('Video_ID', '=', $videoId)->first();
  	DB::table('videos')->where('Video_ID', '=', $videoId)->delete();
  	unlink(BASE_DIR.'/uploads/videos/'.$video['Video_File']);
  	$videos = DB::table('videos')->orderBy('Video_ID', 'desc')->get();
  	if(count($videos) > 0){
  		$res['code'] = 0;
  		$res['text'] = $videos;
  	}else{
  		$res['code'] = 1;
  		$res['text'] = 'No videos are available.';
  	}
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'Video is not seleted.';
  }

  echo json_encode($res);
}

// Audio Related Functions
function UploadAudio(){
  $res = array();

  $json = inputs();

  if(isset($json)){
  	$allowed = array('mp3', 'MP3', 'mpeg', 'MPEG');
  	$audioTitle = $json->title;
  	$audioDesc = $json->desc;
  	if(!isset($json->id)){
  		$audioUpld = date('Y-m-d H:i:s');
  		if(!empty($_FILES)){
  			$audioTmp = $_FILES['file']['tmp_name'];
  			$audioName = $_FILES['file']['name'];
  			$audioNew = time().'-'.$audioName;
  			$audioPath = BASE_DIR.'/uploads/audios/'.$audioNew;
  			$ext = pathinfo($audioName, PATHINFO_EXTENSION);
  			if(in_array($ext, $allowed)){
  				if(move_uploaded_file($audioTmp, $audioPath)){
  					$audioArray = array(
  						'Audio_Title' => $audioTitle,
  						'Audio_Original' => $audioName,
  						'Audio_File' => $audioNew,
  						'Audio_Desc' => $audioDesc,
  						'Audio_UploadOn' => $audioUpld
  					);
  					DB::table('audios')->insert($audioArray);
  					$audios = DB::table('audios')->orderBy('Audio_ID', 'desc')->get();
  					if(count($audios) > 0){
  						$res['code'] = 0;
  						$res['text'] = $audios;
  					}else{
  						$res['code'] = 1;
  						$res['text'] = 'No audios are available.';
  					}
  				}
  			}else{
  				$res['code'] = 2;
  				$res['text'] = 'Audio format is not allowed.';
  			}
  		}
  	}else{
  		$audioId = $json->id;
  		$audio = DB::table('audios')->where('Audio_ID', '=', $audioId)->first();
  		if(!empty($_FILES)){
  			$audioTmp = $_FILES['file']['tmp_name'];
  			$audioName = $_FILES['file']['name'];
  			$audioNew = time().'-'.$audioName;
  			$audioPath = BASE_DIR.'/uploads/audios/'.$audioNew;
  			$ext = pathinfo($audioName, PATHINFO_EXTENSION);
  			if(in_array($ext, $allowed)){
  				if(move_uploaded_file($audioTmp, $audioPath)){
  					$audioArray = array(
  						'Audio_Title' => $audioTitle,
  						'Audio_Original' => $audioName,
  						'Audio_File' => $audioNew,
  						'Audio_Desc' => $audioDesc
  					);
  					DB::table('audios')->where('Audio_ID', '=', $audioId)->update($audioArray);
  					unlink(dirname(dirname(__FILE__)).'/uploads/audios/'.$audio['Audio_File']);
  					$audios = DB::table('audios')->orderBy('Audio_ID', 'desc')->get();
  					if(count($audios) > 0){
  						$res['code'] = 0;
  						$res['text'] = $audios;
  					}else{
  						$res['code'] = 1;
  						$res['text'] = 'No audios are available.';
  					}
  				}
  			}else{
  				$res['code'] = 2;
  				$res['text'] = 'Audio format is not allowed.';
  			}
  		}else{
  			$audioArray = array(
  				'Audio_Title' => $audioTitle,
  				'Audio_Desc' => $audioDesc
  			);
  			DB::table('audios')->where('Audio_ID', '=', $audioId)->update($audioArray);
  			$audios = DB::table('audios')->orderBy('Audio_ID', 'desc')->get();
  			if(count($audios) > 0){
  				$res['code'] = 0;
  				$res['text'] = $audios;
  			}else{
  				$res['code'] = 1;
  				$res['text'] = 'No audios are available.';
  			}
  		}
  	}
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'All fields are required.';
  }

  echo json_encode($res);
}

function GetAllAudios(){
  $res = array();

  $audios = DB::table('audios')->orderBy('Audio_ID', 'desc')->get();
  if(count($audios) > 0){
  	$res['code'] = 0;
  	$res['text'] = $audios;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No audios are available.';
  }

  echo json_encode($res);
}

function GetAudioByID(){
  $res = array();

  $json = inputs();

  $audioId = $json->AudioID;
  $audio = DB::table('audios')->where('Audio_ID', '=', $audioId)->first();
  if(count($audio) > 0){
  	$res['code'] = 0;
  	$res['text'] = $audio;
  } else {
  	$res['code'] = 1;
  	$res['text'] = 'No audio is available.';
  }

  echo json_encode($res);
}

function TrashAudio(){
  $res = array();

  $json = inputs();

  if(!empty($json->audioId)){
  	$audioId = $json->audioId;
  	$audio = DB::table('audios')->where('Audio_ID', '=', $audioId)->first();
  	DB::table('audios')->where('Audio_ID', '=', $audioId)->delete();
  	unlink(BASE_DIR.'/uploads/audios/'.$audio['Audio_File']);
  	$audios = DB::table('audios')->orderBy('Audio_ID', 'desc')->get();
  	if(count($audios) > 0){
  		$res['code'] = 0;
  		$res['text'] = $audios;
  	}else{
  		$res['code'] = 1;
  		$res['text'] = 'No audios are available.';
  	}
  } else {
  	$res['code'] = 2;
  	$res['text'] = 'Audio is not seleted.';
  }

  echo json_encode($res);
}

// Product Related Functions
function CreateEditProduct(){
  $res = array();
  $error = false;

  $json = inputs();

  if(!empty($json->Name) && !empty($json->Desc) && !empty($json->Link)){
  	$productName = $json->Name;
  	$productPrice = $json->Price != 'undefined' ? $json->Price : (int) 0;
  	$productSplPrice = $json->SplPrice != 'undefined' ? $json->SplPrice : (int) 0;
  	$productDesc = $json->Desc;
  	$productLink = $json->Link;
  	$buttonText = $json->ButtonText != 'undefined' ? $json->ButtonText : '';
  	if($productPrice){
  	    if(!is_numeric($productPrice)){
    			$res['code'] = 1;
    			$res['text'] = 'Product price should be an integer/float value.';
    			$error = true;
  	    }
  	}
  	if($productSplPrice){
  	    if(!is_numeric($productSplPrice)){
    			$res['code'] = 2;
    			$res['text'] = 'Special price should be an integer/float value.';
    			$error = true;
  	    }
  	}
  	if($error == false){
  		if(filter_var($productLink, FILTER_VALIDATE_URL) !== FALSE){
  			$productArray = array(
  				'Product_Name' => $productName,
  				'Product_Desc' => $productDesc,
  				'Product_Price' => $productPrice,
  				'Product_Spl_Price' => $productSplPrice,
  				'Product_Link' => $productLink,
  				'Button_Text' => $buttonText
  			);
  			if(empty($json->ProductId)){
  				$productCreateOn = date('Y-m-d H:i:s');
  				$productArray['CreateOn'] = $productCreateOn;
  				DB::table('products')->insert($productArray);
  			}else{
  				$productId = $json->ProductId;
  				DB::table('products')->where('Product_ID', '=', $productId)->update($productArray);
  			}
  			$products = DB::table('products')->orderBy('Product_ID', 'desc')->get();
  			if(count($products) > 0){
  				$res['code'] = 0;
  				$res['text'] = $products;
  			}else{
  				$res['code'] = 3;
  				$res['text'] = 'No products are available.';
  			}
  		}else{
  			$res['code'] = 4;
  			$res['text'] = 'Enter a valid product link.';
  		}
  	}
  }else{
  	$res['code'] = 5;
  	$res['text'] = 'Product name, price, description & buy link are required.';
  }

  echo json_encode($res);
}

function GetAllProducts(){
  $res = array();

  $products = DB::table('products')->orderBy('Product_ID', 'desc')->get();
  if(count($products) > 0){
  	$res['code'] = 0;
  	$res['text'] = $products;
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'No products are available.';
  }

  echo json_encode($res);
}

function TrashProduct(){
  $res = array();

  $json = inputs();

  if(!empty($json->productId)){
  	$productId = $json->productId;
  	DB::table('products')->where('Product_ID', '=', $productId)->delete();
  	$products = DB::table('products')->orderBy('Product_ID', 'desc')->get();
  	if(count($products) > 0){
  		$res['code'] = 0;
  		$res['text'] = $products;
  	}else{
  		$res['code'] = 3;
  		$res['text'] = 'No products are available.';
  	}
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'Product is not seleted.';
  }

  echo json_encode($res);
}

function GetProductByID(){
  $res = array();

  $json = inputs();

  $productId = $json->ProductID;
  $product = DB::table('products')->where('Product_ID', '=', $productId)->first();
  if(count($product) > 0){
  	$res['code'] = 0;
  	$res['text'] = $product;
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'No product is available.';
  }

  echo json_encode($res);
}

// Event Related Functions
function CreateEditEvent(){
  $res = array();
  $error = false;

  $json = inputs();

  if(!empty($json->Name) && !empty($json->Desc) && !empty($json->Venue) && !empty($json->Time) && !empty($json->Link)){
  	$eventName = $json->Name;
  	$eventDesc = $json->Desc;
  	$eventVenue = $json->Venue;
  	$eventTime = $json->Time;
  	$eventPrice = $json->Price != 'undefined' ? $json->Price : (int) 0;
  	$eventLink = $json->Link;
  	$buttonText = $json->ButtonText != 'undefined' ? $json->ButtonText : '';
  	if($eventPrice){
  	    if(!is_numeric($eventPrice)){
    			$res['code'] = 1;
    			$res['text'] = 'Event price should be an integer/float value.';
    			$error = true;
  	    }
  	}
  	if($error == false){
  		if(filter_var($eventLink, FILTER_VALIDATE_URL) !== FALSE){
  			$eventArray = array(
  				'Event_Name' => $eventName,
  				'Event_Desc' => $eventDesc,
  				'Event_Venue' => $eventVenue,
  				'Event_Time' => $eventTime,
  				'Event_Price' => $eventPrice,
  				'Event_Link' => $eventLink,
  				'Button_Text' => $buttonText
  			);
  			if(empty($json->EventId)){
  				$eventCreateOn = date('Y-m-d H:i:s');
  				$eventArray['CreateOn'] = $eventCreateOn;
  				DB::table('events')->insert($eventArray);
  			}else{
  				$eventId = $json->EventId;
  				DB::table('events')->where('Event_ID', '=', $eventId)->update($eventArray);
  			}
  			$events = DB::table('events')->orderBy('Event_ID', 'desc')->get();
  			if(count($events) > 0){
  				$res['code'] = 0;
  				$res['text'] = $events;
  			}else{
  				$res['code'] = 2;
  				$res['text'] = 'No events are available.';
  			}
  		}else{
  			$res['code'] = 3;
  			$res['text'] = 'Enter a valid event link.';
  		}
  	}
  }else{
  	$res['code'] = 4;
  	$res['text'] = 'Event name, description, venue, time & link are required.';
  }

  echo json_encode($res);
}

function GetAllEvents(){
  $res = array();

  $events = DB::table('events')->orderBy('Event_ID', 'desc')->get();
  if(count($events) > 0){
  	$res['code'] = 0;
  	$res['text'] = $events;
  }else{
  	$res['code'] = 2;
  	$res['text'] = 'No events are available.';
  }

  echo json_encode($res);
}

function TrashEvent(){
  $res = array();

  $json = inputs();

  if(!empty($json->eventId)){
  	$eventId = $json->eventId;
  	DB::table('events')->where('Event_ID', '=', $eventId)->delete();
  	$events = DB::table('events')->orderBy('Event_ID', 'desc')->get();
  	if(count($events) > 0){
  		$res['code'] = 0;
  		$res['text'] = $events;
  	}else{
  		$res['code'] = 1;
  		$res['text'] = 'No events are available.';
  	}
  }else{
  	$res['code'] = 2;
  	$res['text'] = 'Event is not seleted.';
  }

  echo json_encode($res);
}

function GetEventByID(){
  $res = array();

  $json = inputs();

  $eventId = $json->EventID;
  $event = DB::table('events')->where('Event_ID', '=', $eventId)->first();
  if(count($event) > 0){
  	$res['code'] = 0;
  	$res['text'] = $event;
  }else{
  	$res['code'] = 2;
  	$res['text'] = 'No event is available.';
  }

  echo json_encode($res);
}

function GetAllSupports(){
  $res = array();

  $supports = DB::table('supports')->orderBy('Support_ID', 'desc')->get();
  if(count($supports) > 0){
  	$i = 0;
  	foreach($supports as $support){
  		$res['text'][$i] = $support;
  		$user = DB::table('users')->where('User_ID', '=', $support['User_ID'])->first();
  		$res['text'][$i]['User_FullName'] = $user['User_FullName'];
  		$i++;
  	}
  	$res['code'] = 0;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No support tickets are available.';
  }

  echo json_encode($res);
}

function TrashSupport(){
  $res = array();

  $json = inputs();

  if(!empty($json->supportId)){
  	$supportId = $json->supportId;
  	DB::table('supports')->where('Support_ID', '=', $supportId)->delete();
  	$supports = DB::table('supports')->orderBy('Support_ID', 'desc')->get();
  	if(count($supports) > 0){
  		$i = 0;
  		foreach($supports as $support){
  			$res['text'][$i] = $support;
  			$user = DB::table('users')->where('User_ID', '=', $support['User_ID'])->first();
  			if(count($user) == 1){
  				$res['text'][$i]['User_FullName'] = $user['User_FullName'];
  			}
  			$i++;
  		}
  		$res['code'] = 0;
  	}else{
  		$res['code'] = 1;
  		$res['text'] = 'No support tickets are available.';
  	}
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'Support is not seleted.';
  }

  echo json_encode($res);
}

function GetSupportByID(){
  $res = array();

  $json = inputs();

  $supportId = $json->SupportID;
  $support = DB::table('supports')->where('Support_ID', '=', $supportId)->first();
  if(count($support) > 0){
  	$user = DB::table('users')->where('User_ID', '=', $support['User_ID'])->first();
  	$support['user'] = $user;
  	$res['code'] = 0;
  	$res['text'] = $support;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No support tickets are available.';
  }

  echo json_encode($res);
}

function ReplySupport(){
  $res = array();

  $json = inputs();

  $email = $json->email;
  $name = $json->name;
  $reply = $json->reply;
  if(!empty($reply)){
  	$subject = 'RE: SpeedDial Support Ticket';
  	$message = $reply;
  	$message .= '<p>Cheers,<br />SpeedDial</p>';
  	Send_Mail($subject, $message, $name, $email);
  	$res['code'] = 0;
  	$res['text'] = 'Reply has been successfully sent.';
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'Please enter your message.';
  }

  echo json_encode($res);
}

function GetAllConsultations(){
  $res = array();

  $consultations = DB::table('consultations')->orderBy('Con_ID', 'desc')->get();
  if(count($consultations) > 0){
  	$i = 0;
  	foreach($consultations as $consultation){
  		$res['text'][$i] = $consultation;
  		$user = DB::table('users')->where('User_ID', '=', $consultation['User_ID'])->first();
  		$res['text'][$i]['User_FullName'] = $user['User_FullName'];
  		$i++;
  	}
  	$res['code'] = 0;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No free consultations are available.';
  }

  echo json_encode($res);
}

function TrashConsult(){
  $res = array();

  $json = inputs();

  if(!empty($json->consultId)){
  	$consultId = $json->consultId;
  	DB::table('consultations')->where('Con_ID', '=', $consultId)->delete();
  	$consultations = DB::table('consultations')->orderBy('Con_ID', 'desc')->get();
  	if(count($consultations) > 0){
  		$i = 0;
  		foreach($consultations as $consultation){
  			$res['text'][$i] = $consultation;
  			$user = DB::table('users')->where('User_ID', '=', $consultation['User_ID'])->first();
  			if(count($user) == 1){
  				$res['text'][$i]['User_FullName'] = $user['User_FullName'];
  			}
  			$i++;
  		}
  		$res['code'] = 0;
  	}else{
  		$res['code'] = 1;
  		$res['text'] = 'No free consultations are available.';
  	}
  }else{
  	$res['code'] = 3;
  	$res['text'] = 'Consultation is not seleted.';
  }

  echo json_encode($res);
}

function GetConsultByID(){
  $res = array();

  $json = inputs();

  $consultId = $json->ConsultID;
  $consultation = DB::table('consultations')->where('Con_ID', '=', $consultId)->first();
  if(count($consultation) > 0){
  	$user = DB::table('users')->where('User_ID', '=', $consultation['User_ID'])->first();
  	$consultation['user'] = $user;
  	$res['code'] = 0;
  	$res['text'] = $consultation;
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'No free consultations are available.';
  }

  echo json_encode($res);
}

function ReplyConsult(){
  $res = array();

  $json = inputs();

  $email = $json->email;
  $name = $json->name;
  $subject = $json->subject;
  $reply = $json->reply;
  if(!empty($reply)){
  	$subject = 'RE: '.$subject;
  	$message = $reply;
  	$message .= '<p>Cheers,<br />SpeedDial</p>';
  	Send_Mail($subject, $message, $name, $email);
  	$res['code'] = 0;
  	$res['text'] = 'Reply has been successfully sent.';
  }else{
  	$res['code'] = 1;
  	$res['text'] = 'Please enter your message.';
  }

  echo json_encode($res);
}


function ScheduleNotification(){
  $res = array();

  $json = inputs();

  if(!$json->time || !$json->title || !$json->link || !$json->message){
    $res['code'] = 1;
    $res['text'] = 'All fields are required.';
    echo json_encode($res);
    return;
  }

  DB::table('push')->insert(array(
    'link' => $json->link,
    'time' => $json->time,
    'title' => $json->title,
    'message' => $json->message,
    'timestamp' => date('Y-m-d H:i:s'),
  ));

  $res['code'] = 0;
  $res['text'] = 'Push notification has been scheduled.';

  echo json_encode($res);
}
