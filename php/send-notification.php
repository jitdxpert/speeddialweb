<?php

$message = $_POST['message'];
$title = $_POST['title'];

$path_to_fcm = 'https://fcm.googleapis.com/fcm/send';
$server_key = "AAAAIrnKcxg:APA91bH9nUl0cK5ezD08qEvfM7kwTgxYdPYr5qOhfA7YdyhB87HaR9zzxRkNw3-QyhUVhmrYimEIVSrqZbMsoDciWACOOp8XO97tlvRG4VaHGpqf-hQVE5mYpVBrxRxbafgf2Rhy83KnyP9napkeEA9zAY9V1Ovvrw";

$sql = DB::table('fcm_token')->get();
$key = $sql[0];

$headers = array(
  'Authorization:key='.$server_key,
  'Content-Type:application/json'
);

$fields = array(
  'to' => $key,
  'notification' => array(
    'title' => $title,
    'body' => $message
  )
);

$payload = json_encode($fields);

$curl_session = curl_init();
curl_setopt($curl_session, CURLOTP_URL, $path_to_fcm);
curl_setopt($curl_session, CURLOTP_POST, true);
curl_setopt($curl_session, CURLOTP_HTTPHEADER, $headers);
curl_setopt($curl_session, CURLOTP_RETURNTRANSFER, true);
curl_setopt($curl_session, CURLOTP_SSL_VERIFYPEER, false);
curl_setopt($curl_session, CURLOTP_IPRESOLVE, CURLOTP_IPRESOLVE_V4);
curl_setopt($curl_session, CURLOTP_POSTFIELDS, $payload);

$result = curl_exec($curl_session);
curl_close($curl_session);
