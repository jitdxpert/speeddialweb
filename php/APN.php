<?php

Class APN{

	public static function send($devices, $payload){
		$port = 2195;
		$passphrase = '';

		$service = 'gateway.push.apple.com';
		$certificate = BASE_DIR.'/php/speeddial-push-notification.pem';

		if(!file_exists($certificate))
			return false;

		$_payload = [];
		$_payload['aps'] = [
			'badge' => 0,
			'sound' => 'default',
			'alert' => $payload['message'],
		];
		// $_payload['body'] = $payload['message'];
		// $_payload['title'] = $payload['title'];
		// $_payload['link'] = $payload['link'];
		$_payload['all'] = $payload;
		$_payload = json_encode($_payload);

		$streamContext = stream_context_create();
		stream_context_set_option($streamContext, 'ssl', 'local_cert', $certificate);
		stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);

		$apns = stream_socket_client('ssl://'.$service.':'.$port, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);

		if(!$apns)
			return false;

		foreach($devices as $key => $token){
			unset($devices[$key]);

			$token = pack('H*', str_replace(' ', '', $token));
			$_payload = chr(0).chr(0).chr(32).$token.chr(0).chr(strlen($_payload)).$_payload;

			$response = fwrite($apns, $_payload);
			if($response != 92){
				sleep(2);
				fclose($apns);
				return self::send($devices, $payload);
			}
		}
		fclose($apns);

		return true;
	}

}
