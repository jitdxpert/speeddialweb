<?php

function Send_Mail($subject, $message, $toName, $toMail, $fromName=null, $fromMail=null, $cc=null, $bcc=null, $attachment=null, $debug=null){
  global $PHPMailer;

  $PHPMailer->ClearAllRecipients();

  $PHPMailer->isSMTP();
  $PHPMailer->SMTPAuth = TRUE;
  $PHPMailer->Host = PHPMAILER_HOST;
  $PHPMailer->Port = PHPMAILER_PORT;
  $PHPMailer->Username = PHPMAILER_USERNAME;
  $PHPMailer->Password = PHPMAILER_PASSWORD;
  $PHPMailer->SMTPSecure = PHPMAILER_SMTPSECURE;

  if($fromName && $fromMail){
    $PHPMailer->From = $fromMail;
    $PHPMailer->FromName = $fromName;
  }else{
    $PHPMailer->From = PHPMAILER_FROM;
    $PHPMailer->FromName = PHPMAILER_FROMNAME;
  }

  if(is_array($toName) && is_array($toMail)){
    for($i = 0; $i < count($toMail); $i++){
      $PHPMailer->addAddress($toMail[$i], $toName[$i]);
    }
  }else{
    $PHPMailer->addAddress($toMail, $toName);
  }

  if($fromName && $fromMail){
    $PHPMailer->addReplyTo($fromMail, $fromName);
  }else{
    $PHPMailer->addReplyTo(PHPMAILER_FROM, PHPMAILER_FROMNAME);
  }

  if($cc){
    if(is_array($cc)){
      for($i = 0; $i < count($cc); $i++){
        $PHPMailer->addCC($cc[$i]);
      }
    }else{
      $PHPMailer->addCC($cc);
    }
  }

  if($bcc){
    if(is_array($bcc)){
      for($i = 0; $i < count($bcc); $i++){
        $PHPMailer->addBCC($bcc[$i]);
      }
    }else{
      $PHPMailer->addBCC($bcc);
    }
  }

  $PHPMailer->WordWrap = PHPMAILER_WORDWRAP;

  if($attachment){
    for($i = 0; $i < count($attachment); $i++){
      $PHPMailer->addAttachment($attachment[$i]['src'], $attachment[$i]['name']);
    }
  }

  $PHPMailer->isHTML(TRUE);

  if($debug){
    $PHPMailer->SMTPDebug = 2;
  }

  $PHPMailer->Subject = $subject;
  $PHPMailer->Body = $message;

  if(!$PHPMailer->send()){
    return 1;
  }else{
    return 0;
  }
}

function push_to_ios($data, $token){
  $params = array();
  $params['teamId'] = 'MG4Y3EEQKV';
  $params['authKeyId'] = 'G92FF4SU33';
  $params['apns-topic'] = 'com.mobiblocks.JeannaGabellini';
  $params['a8_key'] = '-----BEGIN PRIVATE KEY-----
MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgEscg36gYcnNbUOsl
s85BgOwFpYHeL4/Qh327Pr83dxagCgYIKoZIzj0DAQehRANCAARO96TTmNTfYHPl
bb0kISQ5r78Jt3BbzWeE704BLbiHuKF7ggGBH1FSWS4ywpIlG/K6mkgKhsaiya8S
KG9RMsmx
-----END PRIVATE KEY-----';
  $claim = array(
  	'iss' => $params['teamId'],
  	'iat' => time()
  );

  $params['header_jwt'] = JWT::encode($claim, $params['a8_key'], $params['authKeyId'], 'RS256');

  $endPoint = 'https://api.push.apple.com/3/device';

  $headers = array();
  $headers[] = sprintf("content-type: application/json");
  $headers[] = sprintf("authorization: bearer %s", $params['header_jwt']);
  $headers[] = sprintf("apns-topic: %s", $params['apns-topic']);

  $fields = array();
  $fields['aps']['alert']['title'] = sprintf($data['title']);
  $fields['aps']['alert']['body'] = sprintf($data['message']);
  $fields['aps']['alert']['link'] = sprintf($data['link']);

  $url = sprintf("%s/%s", $endPoint, $token);
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
  curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $result = curl_exec($ch);
  curl_close($ch);

  return $result;
}

function push_to_ios_2($data, $token){
  $devices[] = $token;

  $apn = new APN;
  return $apn->send($devices, $data);
}

function push_to_android($data, $token){
  $serverKey = "AIzaSyBZ7IoxKlsIkgvdewnhxCWJ6opb0oxGp1I";
  $headers = array(
    'Authorization:key='.$serverKey,
    'Content-Type:application/json'
  );

  $fields = array(
    "to" => $token,
    "notification" => array(
      "title" => $data['title'],
      "body" => $data['message'],
      "sound" => "default",
      "icon"  => "icon",
      "color" => "#e05729",
    ),
    "data" => array(
      "title" => $data['title'],
      "body" => $data['message'],
      "link" => $data['link'],
    ),
  );

  $endPoint = 'https://fcm.googleapis.com/fcm/send';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $endPoint);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
  $result = curl_exec($ch);
  curl_close($ch);

  return $result;
}

function send_notification($push, $users){
  $send = array();

  $push = (object)$push;

  if(!$push->title || !$push->message){
    return 'error';
  }

  $data = array(
    'link' => $push->link,
    'title' => $push->title,
    'message' => $push->message,
  );

  $tokens = array();
  foreach($users as $user){
    $device = $user['User_Device'];
    $token = $user['Registration_ID'];

    if(!$device || !$token){
        continue;
    }


    if(strtolower($device) == 'android'){
      $send['android'][] = push_to_android($data, $token);
    }elseif(strtolower($device) == 'ios'){
      $send['ios'][] = push_to_ios($data, $token);
    }
  }

  return $send;
}

function storeRocks($rockId, $rockNo, $rock, $date, $journalId, $userId){
  $rockArray = array(
    'Answer' => $rock,
    'User_ID' => $userId,
    'Rock_No' => $rockNo,
    'Rock_Date' => $date,
    'Journal_ID' => $journalId,
  );
  $checkRock = DB::table('rocks')->where('User_ID', '=', $userId)->where('Rock_Date', '=', $date)->where('Rock_No', '=', $rockNo)->count();
  if(!$checkRock){
    $rockArray['CreateOn'] = date('Y-m-d H:i:s');
    DB::table('rocks')->insert($rockArray);
  }else{
    DB::table('rocks')
    ->where('Rock_ID', '=', $rockId)
    ->where('Rock_Date', '=', $date)
    ->where('Rock_No', '=', $rockNo)
    ->where('Journal_ID', '=', $journalId)
    ->where('User_ID', '=', $userId)
    ->update($rockArray);
  }
}

function inputs(){
  $json = file_get_contents('php://input');
  $json = $json ? $json : '{}';
  $json = json_decode($json);
  if(!$json){
    $json = [];
  }

  $inputs = array_merge($_GET, $_POST, (array)$json);
  return (object)$inputs;
}

function setUserTimeZone($tzOffset){
  $tzOffset = explode('.', $tzOffset);
  $offsetMinutes = ($tzOffset[1] / 10) * 60;
  $offset = $tzOffset[0].':'.$offsetMinutes;

  list($hours, $minutes) = explode(':', $offset);
  $seconds = $hours * 60 * 60 + $minutes * 60;
  $tz = timezone_name_from_abbr('', $seconds, 1);
  if($tz === false) $tz = timezone_name_from_abbr('', $seconds, 0);

  date_default_timezone_set($tz);
}

function render($view, $data = array()){
  if(file_exists(BASE_DIR . $view)){
    extract($data);
    ob_start();
    include BASE_DIR . $view;
    $output = ob_get_clean();
  }else{
    $output = 'View not exists.';
  }
  return $output;
}

function d($var, $option='json'){
  debug($var, $option);
}

function debug($var, $option='json'){
  echo "<pre>";
  $die = true;
  if($option=='php|continue' || $option=='p|c'){
    $die = false;
    $response = var_export($var);
  }elseif($option=='json|continue' || $option=='j|c'){
    $die = false;
    $response = json_encode($var);
  }elseif($option=='continue' || $option=='c'){
    $die = false;
    $response = json_encode($var);
  }elseif($option=='php' || $option=='p'){
    $response = var_export($var);
  }else{
    $response = json_encode($var);
  }

  RESPONSE:
  echo $response; if($die) die;
}
