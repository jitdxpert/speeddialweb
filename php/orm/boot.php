<?php

require 'vendor/autoload.php';  
 
use Illuminate\Database\Capsule\Manager as Capsule;  
 
$capsule = new Capsule; 
 
$capsule->addConnection(array(
    'driver'    => 'mysql',
    'host'      => DB_HOST,
    'database'  => DB_NAME,
    'username'  => DB_USER,
    'password'  => DB_PASSWORD,
    'charset'   => DB_CHARSET,
    'collation' => DB_COLLATE,
    'prefix'    => DB_PREFIX,
));
 
$capsule->setAsGlobal();
$capsule->bootEloquent();

class DB extends Capsule{

}
