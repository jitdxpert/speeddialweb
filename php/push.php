<?php

error_reporting(-1);
ini_set('display_errors', 'On');

include(dirname(__FILE__).'/config.php');

$output = [];
$userId = 0;
$inputs = inputs();
if($inputs){
    $pushId = isset($inputs->push_id) ? $inputs->push_id : 0;
    $userId = isset($inputs->user_id) ? $inputs->user_id : 0;
}


if($pushId){
    $pushes = DB::table('push')->where('id', '=', $pushId)->get();
}else{
    $pushes = DB::table('push')->whereNull('sent_on')->where('time', '<=', date('Y-m-d H:i:s'))->get();
}

if(!count($pushes)){
    d('No schedule push notifications.');
}


if($userId){
    $users = DB::table('users')->where('User_ID', $userId)->get();
}else{
    $users = DB::table('users')->get();
}

foreach($pushes as $key => $push){
    $send = send_notification($push, $users);
    if($send != 'error'){
        DB::table('push')->where('id', '=', $push['id'])
        ->update(array(
            'sent_on' => date('Y-m-d H:i:s'),
        ));
    }
    $output[] = $send;
}

d($output);
