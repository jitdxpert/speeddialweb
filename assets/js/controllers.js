'use strict';

app.controller('LoginCtrl', function($scope, $http, $ngConfirm, $location, loginService, sessionService){
	$scope.LoginProcess = function(signin){
		loginService.login(signin, $scope).then(function(res){
			if(res.code == 0){
				sessionService.set('admin', res.text);
				$location.path('/users');
			}else{
				$ngConfirm({
					boxWidth: '20%',
					useBootstrap: false,
					title: false,
					content: res.text,
					type: 'red',
					typeAnimated: true,
					buttons: {
						close: {
							btnClass: 'btn-red',
							keys: ['enter', 'esc'],
						}
					}
				});
			}
		}, function(error){
			$ngConfirm({
				boxWidth: '20%',
				useBootstrap: false,
				title: false,
				content: error,
				type: 'red',
				typeAnimated: true,
				buttons: {
					close: {
						btnClass: 'btn-red',
						keys: ['enter', 'esc'],
					}
				}
			});
		});
	};
});

app.controller('MenuCtrl', function($scope, $location, loginService){
	$scope.logout = function(){
		loginService.logout();
	};

	$scope.states = {};
	$scope.states.activeItem = $location.path();
	$scope.menuClick = function(menuItem){
		$scope.states.activeItem = menuItem;
		$location.path(menuItem);
	};
	$scope.menus = [{
		href: '/users',
		name: 'Users'
	}, {
		href: '/content',
		name: 'App Content'
	}, {
		href: '/media',
		name: 'Media'
	}, {
		href: '/events',
		name: 'Events'
	}, {
		href: '/products',
		name: 'Products'
	}, {
		href: '/support',
		name: 'Help & Consultation'
	}];
});

app.controller('ContentCtrl', function($scope, $http, $timeout){
	$scope.options = {
		language: 'en',
		allowedContent: true,
		entities: false
	};

	$scope.sd = {
		page: 'SpeedDial',
		content: ''
	};
	$scope.au = {
		page: 'AboutUs',
		content: ''
	};
	$scope.ns = {
		page: 'NotificationTime',
		content: ''
	};
	$scope.fb = {
		page: 'FreeButton',
		content: ''
	};
	$scope.bb = {
		page: 'BuyButton',
		content: ''
	};
	$scope.rbc = {
		page: 'ResourceButtonColor',
		content: ''
	};
	$scope.ce = {
		page: 'ConsultEmailBody',
		content: ''
	};
	$scope.se = {
		page: 'SupportEmailBody',
		content: ''
	};

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.sd.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.sd.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.au.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.au.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.ns.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.ns.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.fb.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.fb.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.bb.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.bb.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.rbc.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.rbc.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.ce.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.ce.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.se.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.pageContentError = '';
			$scope.se.content = unescape(res.data.text.Page_Content);
		}
	}, function(error){
		$scope.pageContentSuccess = '';
		$scope.pageContentError = error;
	});

	$scope.SavePageContent = function(content){
		$http({
			url: 'php/?case=SavePageContent',
			method: 'POST',
			data: 'page='+content.page+'&content='+escape(content.content),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			}
		}).then(function(res){
			if(res.data.code == 0){
				$scope.pageContentError = '';
				$scope.pageContentSuccess = res.data.text;
				$timeout(function(){
					$scope.pageContentSuccess = '';
				}, 3000);
			}else{
				$scope.pageContentSuccess = '';
				$scope.pageContentError = res.data.text;
			}
		}, function(error){
			$scope.pageContentSuccess = '';
			$scope.pageContentError = error;
		});
	};

	var d = new Date();
	var time = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
	$scope.pn = {
		time: time,
		title: '',
		link: '',
		desc: '',
	};
	$scope.SchedulePushNotification = function(push){
		$http({
			url: 'php/?case=ScheduleNotification',
			method: 'POST',
			data: 'time='+push.time+'&title='+push.title+'&link='+push.link+'&message='+push.desc,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			}
		}).then(function(res){
			if(res.data.code == 0){
				$scope.pageContentError = '';
				$scope.pageContentSuccess = res.data.text;
				push.time = time;
				push.title = '';
				push.link = '';
				push.desc = '';
				$timeout(function(){
					$scope.pageContentSuccess = '';
				}, 3000);
			}else{
				$scope.pageContentSuccess = '';
				$scope.pageContentError = res.data.text;
			}
		}, function(error){
			$scope.pageContentSuccess = '';
			$scope.pageContentError = error;
		});
	};

	var tabClasses;
	function initTabs(){
		tabClasses = ["","",""];
	}
	$scope.getTabClass = function(tabNum){
		return tabClasses[tabNum];
	};
	$scope.getTabPaneClass = function(tabNum){
		return "tab-pane "+tabClasses[tabNum];
	}
	$scope.setActiveTab = function(tabNum){
		initTabs();
		tabClasses[tabNum] = "active";
	};

	initTabs();
	$scope.setActiveTab(1);
});

app.controller('UsersCtrl', function($scope, $http, $ngConfirm){
	// Get all users
	$scope.userListError = '';
	$scope.userList = [];
	var tmpUserList = [];
	$scope.UsersCount = 0;
	$scope.UsersLast7Days = 0;
	$scope.MonthlyActiveUsers = 0;
	$scope.AVGTimeSpent = 0;
	getUsers();

	$scope.filterOn = false;
	$scope.clearFilter = function(){
		getUsers();
		$scope.productFilter = '';
		$scope.eventFilter = '';
		$scope.logFilter = '';
	}

	function getUsers(){
		$http({
			url: 'php/?case=GetAllUsers',
			method: 'GET',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			}
		}).then(function(res){
			if(res.data.code == 0){
				$scope.UsersCount = res.data.UsersCount;
				$scope.UsersLast7Days = res.data.UsersLast7Days;
				$scope.MonthlyActiveUsers = res.data.MonthlyActiveUsers;
				$scope.AVGTimeSpent = res.data.AvgSessionTime;
				$scope.userList = res.data.text;
				tmpUserList = res.data.text;
				$scope.userListError = '';
			}else{
				$scope.userList = [];
				$scope.userListError = res.data.text;
			}
			$scope.filterOn = false;
		}, function(error){
			$scope.userList = [];
			$scope.userListError = error;
		});
	}

	// Get all products
	$scope.productList = [];
	$http({
		url: 'php/?case=GetAllProducts',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.productList = res.data.text;
		}
	}, function(error){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: error,
			type: 'red',
			typeAnimated: true,
			buttons: {
				close: {
					btnClass: 'btn-red',
					keys: ['enter', 'esc'],
				}
			}
		});
	});

	// Get all events
	$scope.eventList = [];
	$http({
		url: 'php/?case=GetAllEvents',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.eventList = res.data.text;
		}
	}, function(error){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: error,
			type: 'red',
			typeAnimated: true,
			buttons: {
				close: {
					btnClass: 'btn-red',
					keys: ['enter', 'esc'],
				}
			}
		});
	});

	// Filters
	$scope.userFilter = function(filter, type){
		if(type == 'login'){
			$scope.userList = tmpUserList;
			$scope.userList = $scope.userList.filter(function(item){
				var logTime = item.AvgLogTime+'';
				logTime = logTime.replace(/\,/g,'');
				logTime = Number(logTime);
				if(filter == 1){
					return logTime <= 99;
				}else if(filter == 2){
					return logTime >= 100 && logTime <= 199;
				}else if(filter == 3){
					return logTime >= 200 && logTime <= 499;
				}else if(filter == 4){
					return logTime >= 500 && logTime <= 999;
				}else if(filter == 5){
					return logTime >= 1000;
				}
			});
			if($scope.userList.length){
				$scope.userListError = '';
			}else{
				$scope.userList = [];
				$scope.userListError = 'No users are available.';
			}
		}else{
			$http({
				url: 'php/?case=GetAllUsers',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Cache-Control' : 'no-cache'
				},
				data: 'filter='+filter+'&type='+type
			}).then(function(res){
				if(res.data.code == 0){
					$scope.userList = res.data.text;
					$scope.userListError = '';
				}else{
					$scope.userList = [];
					$scope.userListError = res.data.text;
				}
			}, function(error){
				$scope.userList = [];
				$scope.userListError = error;
			});
		}
		$scope.filterOn = true;
	};
});

app.controller('MediaCtrl', function($scope, $http, $ngConfirm, $window){
	$scope.UploadBtn = 'SAVE';
	$scope.VideoModalTitle = 'Upload New Video';
	// Upload video script
	$scope.videoTitle = '';
	$scope.videoFile  = '';
	$scope.videoDesc  = '';
	$scope.videoId    = '';
	$scope.videoError   = '';
	$scope.videoSuccess = '';
	$scope.UploadVideo = function(){
		if($scope.videoId == ''){
			// new video upload
			if($scope.videoFile && $scope.videoTitle && $scope.videoDesc){
				var fd = new FormData();
				angular.forEach($scope.videoFile, function(file){
					fd.append('file', file);
				});
				fd.append('title', $scope.videoTitle);
				fd.append('desc',  $scope.videoDesc);
				$http({
					url: 'php/?case=UploadVideo',
					method: 'POST',
					data: fd,
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined,
						'Cache-Control' : 'no-cache'
					}
				}).then(function(res){
					if(res.data.code == 0){
						$scope.ClearForm();
						$scope.videoList = res.data.text;
						$scope.videoSuccess = 'Video has been successfully uploaded.';
						$scope.videoError = '';
					}else{
						$scope.videoSuccess = '';
						$scope.videoError = res.data.text;
					}
				}, function(error){
					$scope.videoSuccess = '';
					$scope.videoError = error;
				});
			}else{
				$scope.videoSuccess = '';
				$scope.videoError = 'All fields are required.';
			}
		}else{
			// edit video upload
			if($scope.videoTitle && $scope.videoDesc){
				var fd = new FormData();
				if($scope.videoFile != ''){
					angular.forEach($scope.videoFile, function(file){
						fd.append('file', file);
					});
				}
				fd.append('title', $scope.videoTitle);
				fd.append('desc',  $scope.videoDesc);
				fd.append('id',    $scope.videoId);
				$http({
					url: 'php/?case=UploadVideo',
					method: 'POST',
					data: fd,
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined,
						'Cache-Control' : 'no-cache'
					}
				}).then(function(res){
					if(res.data.code == 0){
						$scope.videoList = res.data.text;
						$scope.videoSuccess = 'Video has been successfully updated.';
						$scope.videoError = '';
					}else{
						$scope.videoSuccess = '';
						$scope.videoError = res.data.text;
					}
				}, function(error){
					$scope.videoSuccess = '';
					$scope.videoError = error;
				});
			}else{
				$scope.videoSuccess = '';
				$scope.videoError = 'Video title, file & description are required.';
			}
		}
	};

	// Edit videos
	$scope.OpenVideoModalForEdit = function(videoId){
		$scope.UploadBtn = 'UPDATE';
		$http({
			url: 'php/?case=GetVideoByID',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			},
			data: 'VideoID='+videoId
		}).then(function(res){
			if(res.data.code == 0){
				$scope.videoTitle = res.data.text.Video_Title;
				$scope.videoDesc  = res.data.text.Video_Desc;
				$scope.videoId    = res.data.text.Video_ID;
				$scope.VideoModalTitle = 'Edit Video';
				$('#videoModal').modal('show');
			}else{
				$scope.productListError = res.data.text;
			}
		}, function(error){
			$scope.productListError = error;
		});
	};

	$scope.TrashVideo = function(videoId){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: 'Are you sure to delete this video?',
			type: 'red',
			typeAnimated: true,
			buttons: {
				yes: {
					btnClass: 'btn-red',
					keys: ['enter'],
					action: function(scope, button){
						$http({
							url: 'php/?case=TrashVideo',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Cache-Control' : 'no-cache'
							},
							data: 'videoId='+videoId
						}).then(function(res){
							if(res.data.code == 0){
								$scope.videoList = res.data.text;
							}else{
								$scope.videoList = [];
								$scope.videoListError = res.data.text;
							}
						}, function(error){
							$ngConfirm({
								boxWidth: '20%',
								useBootstrap: false,
								title: false,
								content: error,
								type: 'red',
								typeAnimated: true,
								buttons: {
									close: {
										btnClass: 'btn-red',
										keys: ['enter', 'esc'],
									}
								}
							});
						});
					}
				},
				no: {
					keys: ['esc']
				},
			}
		});
	};

	$scope.AudioModalTitle = 'Upload New Audio';
	// Upload audios script
	$scope.audioTitle = '';
	$scope.audioFile  = '';
	$scope.audioDesc  = '';
	$scope.audioId    = '';
	$scope.audioError   = '';
	$scope.audioSuccess = '';
	$scope.UploadAudio = function(){
		if($scope.audioId == ''){
			// new video upload
			if($scope.audioFile && $scope.audioTitle && $scope.audioDesc){
				var fd = new FormData();
				angular.forEach($scope.audioFile, function(file){
					fd.append('file', file);
				});
				fd.append('title', $scope.audioTitle);
				fd.append('desc',  $scope.audioDesc);
				$http({
					url: 'php/?case=UploadAudio',
					method: 'POST',
					data: fd,
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined,
						'Cache-Control' : 'no-cache'
					}
				}).then(function(res){
					if(res.data.code == 0){
						$scope.ClearForm();
						$scope.audioList = res.data.text;
						$scope.audioSuccess = 'Audio has been successfully uploaded.';
						$scope.audioError = '';
					}else{
						$scope.audioSuccess = '';
						$scope.audioError = res.data.text;
					}
				}, function(error){
					$scope.audioSuccess = '';
					$scope.audioError = error;
				});
			}else{
				$scope.audioSuccess = '';
				$scope.audioError = 'All fields are required.';
			}
		}else{
			// edit audio upload
			if($scope.audioTitle && $scope.audioDesc){
				var fd = new FormData();
				if($scope.audioFile != ''){
					angular.forEach($scope.audioFile, function(file){
						fd.append('file', file);
					});
				}
				fd.append('title', $scope.audioTitle);
				fd.append('desc',  $scope.audioDesc);
				fd.append('id',    $scope.audioId);
				$http({
					url: 'php/?case=UploadAudio',
					method: 'POST',
					data: fd,
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined,
						'Cache-Control' : 'no-cache'
					}
				}).then(function(res){
					if(res.data.code == 0){
						$scope.audioList = res.data.text;
						$scope.audioSuccess = 'Audio has been successfully updated.';
						$scope.audioError = '';
					}else{
						$scope.audioSuccess = '';
						$scope.audioError = res.data.text;
					}
				}, function(error){
					$scope.audioSuccess = '';
					$scope.audioError = error;
				});
			}else{
				$scope.audioSuccess = '';
				$scope.audioError = 'Audio title & description are required.';
			}
		}
	};

	// Edit videos
	$scope.OpenAudioModalForEdit = function(audioId){
		$scope.UploadBtn = 'UPDATE';
		$http({
			url: 'php/?case=GetAudioByID',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			},
			data: 'AudioID='+audioId
		}).then(function(res){
			if(res.data.code == 0){
				$scope.audioTitle = res.data.text.Audio_Title;
				$scope.audioDesc  = res.data.text.Audio_Desc;
				$scope.audioId    = res.data.text.Audio_ID;
				$scope.AudioModalTitle = 'Edit Audio';
				$('#audioModal').modal('show');
			}else{
				$scope.productListError = res.data.text;
			}
		}, function(error){
			$scope.productListError = error;
		});
	};

	$scope.TrashAudio = function(audioId){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: 'Are you sure to delete this audio?',
			type: 'red',
			typeAnimated: true,
			buttons: {
				yes: {
					btnClass: 'btn-red',
					keys: ['enter'],
					action: function(scope, button){
						$http({
							url: 'php/?case=TrashAudio',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Cache-Control' : 'no-cache'
							},
							data: 'audioId='+audioId
						}).then(function(res){
							if(res.data.code == 0){
								$scope.audioList = res.data.text;
							}else{
								$scope.audioList = [];
								$scope.audioListError = res.data.text;
							}
						}, function(error){
							$ngConfirm({
								boxWidth: '20%',
								useBootstrap: false,
								title: false,
								content: error,
								type: 'red',
								typeAnimated: true,
								buttons: {
									close: {
										btnClass: 'btn-red',
										keys: ['enter', 'esc'],
									}
								}
							});
						});
					}
				},
				no: {
					keys: ['esc']
				},
			}
		});
	};

	// Clear form data
	$scope.ClearForm = function(){
		angular.element("[type='file']").val(null);

		$scope.videoTitle = '';
		$scope.videoDesc  = '';
		$scope.videoId    = '';

		$scope.audioTitle = '';
		$scope.audioDesc  = '';
		$scope.audioId    = '';

		$scope.videoError = '';
		$scope.audioError = '';
		$scope.videoSuccess = '';
		$scope.audioSuccess = '';
		$scope.VideoModalTitle = 'Upload New Video';
		$scope.AudioModalTitle = 'Upload New Audio';
		$scope.UploadBtn = 'SAVE';
	};

	// Get all videos
	$scope.videoListError = '';
	$scope.videoList = [];
	$http({
		url: 'php/?case=GetAllVideos',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.videoList = res.data.text;
		}else{
			$scope.videoListError = res.data.text;
		}
	}, function(error){
		$scope.videoListError = error;
	});

	// Get all audios
	$scope.audioListError = '';
	$scope.audioList = [];
	$http({
		url: 'php/?case=GetAllAudios',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.audioList = res.data.text;
		}else{
			$scope.audioListError = res.data.text;
		}
	}, function(error){
		$scope.audioListError = error;
	});
});

app.controller('EventsCtrl', function($scope, $http, $ngConfirm, $window){
	$scope.EventBtn = 'SAVE';
	$scope.EventModalTitle = 'Add New Event';
	// Create event
	$scope.eventError   = '';
	$scope.eventSuccess = '';
	$scope.EventID 	    = '';
	$scope.CreateEditEvent = function(){
		if($scope.Name && $scope.Desc && $scope.Venue && $scope.Time && $scope.Link){
			var eventId = $scope.EventID;
			var eventTime = formatDate(new Date($scope.Time));
			$http({
				url: 'php/?case=CreateEditEvent',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Cache-Control' : 'no-cache'
				},
				data: 'EventId='+$scope.EventID+'&Name='+$scope.Name+'&Desc='+$scope.Desc+'&Venue='+$scope.Venue+'&Time='+eventTime+'&Price='+$scope.Price+'&Link='+$scope.Link+'&ButtonText='+$scope.ButtonText
			}).then(function(res){
				if(res.data.code == 0){
					$scope.ClearForm();
					if(eventId){
						$scope.eventSuccess = 'Event has been successfully updated.';
					}else{
						$scope.eventSuccess = 'Event has been successfully saved.';
					}
					$scope.eventList = res.data.text;
					$scope.eventError = '';
				}else{
					$scope.eventError = res.data.text;
					$scope.eventSuccess = '';
				}
			}, function(error){
				$scope.eventError = error;
				$scope.eventSuccess = '';
			});
		}else{
			$scope.eventError = 'Event name, description, venue, time & link are required.';
			$scope.eventSuccess = '';
		}
	};

	function formatDate(date){
		var years = date.getFullYear();

		var months = date.getMonth();
		months = months < 10 ? '0'+months : months;

		var days = date.getDate();
		days = days < 10 ? '0'+days : days;

		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12;

		minutes = minutes < 10 ? '0'+minutes : minutes;

		var strTime = hours+':'+minutes+' '+ampm;

		return (parseInt(months)+parseInt(1))+"/"+days+'/'+years+" "+strTime;
	}

	// Clear form data
	$scope.ClearForm = function(){
		$scope.EventID   = '';
		$scope.Name 	 = '';
		$scope.Desc 	 = '';
		$scope.Venue 	 = '';
		angular.element("#event_time").find("[type='text']").val(null);
		$scope.Price 	 = '';
		$scope.Link 	 = '';
		$scope.ButtonText = '';
		$scope.eventError   = '';
		$scope.eventSuccess = '';
		$scope.EventModalTitle = 'Add New Event';
		$scope.EventBtn = 'SAVE';
	};

	// Get all products
	$scope.eventListError = '';
	$scope.eventList = [];
	$http({
		url: 'php/?case=GetAllEvents',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.eventList = res.data.text;
		}else{
			$scope.eventListError = res.data.text;
		}
	}, function(error){
		$scope.eventListError = error;
	});

	// Edit products
	$scope.OpenModalForEdit = function(eventId){
		$scope.EventBtn = 'UPDATE';
		$http({
			url: 'php/?case=GetEventByID',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			},
			data: 'EventID='+eventId
		}).then(function(res){
			if(res.data.code == 0){
				$scope.EventID = res.data.text.Event_ID;
				$scope.Name    = res.data.text.Event_Name;
				$scope.Desc    = res.data.text.Event_Desc;
				$scope.Venue   = res.data.text.Event_Venue;
				$scope.Time    = res.data.text.Event_Time;
				$scope.Price   = res.data.text.Event_Price;
				$scope.Link    = res.data.text.Event_Link;
				$scope.ButtonText = res.data.text.Button_Text;
				$scope.EventModalTitle = 'Edit Event';
				$('#eventModal').modal('show');
			}else{
				$scope.eventListError = res.data.text;
			}
		}, function(error){
			$scope.eventListError = error;
		});
	};

	$scope.TrashEvent = function(eventId){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: 'Are you sure to delete this event?',
			type: 'red',
			typeAnimated: true,
			buttons: {
				yes: {
					btnClass: 'btn-red',
					keys: ['enter'],
					action: function(scope, button){
						$http({
							url: 'php/?case=TrashEvent',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Cache-Control' : 'no-cache'
							},
							data: 'eventId='+eventId
						}).then(function(res){
							if(res.data.code == 0){
								$scope.eventList = res.data.text;
							}else{
								$scope.eventList = [];
								$scope.eventListError = res.data.text;
							}
						}, function(error){
							$ngConfirm({
								boxWidth: '20%',
								useBootstrap: false,
								title: false,
								content: error,
								type: 'red',
								typeAnimated: true,
								buttons: {
									close: {
										btnClass: 'btn-red',
										keys: ['enter', 'esc'],
									}
								}
							});
						});
					}
				},
				no: {
					keys: ['esc']
				},
			}
		});
	};
});

app.controller('ProductsCtrl', function($scope, $http, $ngConfirm, $window){
	$scope.ProductBtn = 'SAVE';
	$scope.ProductModalTitle = 'Add New Product';
	// Create product
	$scope.productError   = '';
	$scope.productSuccess = '';
	$scope.ProductID 	  = '';
	$scope.CreateEditProduct = function(){
		if($scope.Name && $scope.Desc && $scope.Link){
			var productId = $scope.ProductID;
			$http({
				url: 'php/?case=CreateEditProduct',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Cache-Control' : 'no-cache'
				},
				data: 'ProductId='+$scope.ProductID+'&Name='+$scope.Name+'&Desc='+$scope.Desc+'&Price='+$scope.Price+'&SplPrice='+($scope.SplPrice ? $scope.SplPrice : 0)+'&Link='+$scope.Link+'&ButtonText='+$scope.ButtonText
			}).then(function(res){
				if(res.data.code == 0){
					$scope.ClearForm();
					if(productId){
						$scope.productSuccess = 'Product has been successfully updated.';
					}else{
						$scope.productSuccess = 'Product has been successfully saved.';
					}
					$scope.productList = res.data.text;

					$scope.productError = '';
				}else{
					$scope.productError = res.data.text;
					$scope.productSuccess = '';
				}
			}, function(error){
				$scope.productError = error;
				$scope.productSuccess = '';
			});
		}else{
			$scope.productError = 'Product name, description & buy link are required.';
			$scope.productSuccess = '';
		}
	};

	// Clear form data
	$scope.ClearForm = function(){
		$scope.ProductID = '';
		$scope.Name 	 = '';
		$scope.Desc 	 = '';
		$scope.Price 	 = '';
		$scope.SplPrice  = '';
		$scope.Link  	 = '';
		$scope.ButtonText = '';
		$scope.productError   = '';
		$scope.productSuccess = '';
		$scope.ProductModalTitle = 'Add New Product';
		$scope.ProductBtn = 'SAVE';
	};

	// Get all products
	$scope.productListError = '';
	$scope.productList = [];
	$http({
		url: 'php/?case=GetAllProducts',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.productList = res.data.text;
		}else{
			$scope.productListError = res.data.text;
		}
	}, function(error){
		$scope.productListError = error;
	});

	// Edit products
	$scope.OpenModalForEdit = function(productId){
		$scope.ProductBtn = 'UPDATE';
		$http({
			url: 'php/?case=GetProductByID',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			},
			data: 'ProductID='+productId
		}).then(function(res){
			if(res.data.code == 0){
				$scope.ProductID = res.data.text.Product_ID;
				$scope.Name 	 = res.data.text.Product_Name;
				$scope.Desc 	 = res.data.text.Product_Desc;
				$scope.Price 	 = res.data.text.Product_Price;
				$scope.SplPrice  = res.data.text.Product_Spl_Price;
				$scope.Link  	 = res.data.text.Product_Link;
				$scope.ButtonText = res.data.text.Button_Text;
				$scope.ProductModalTitle = 'Edit Product';
				$('#productModal').modal('show');
			}else{
				$scope.productListError = res.data.text;
			}
		}, function(error){
			$scope.productListError = error;
		});
	};

	$scope.TrashProduct = function(productId){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: 'Are you sure to delete this product?',
			type: 'red',
			typeAnimated: true,
			buttons: {
				yes: {
					btnClass: 'btn-red',
					keys: ['enter'],
					action: function(scope, button){
						$http({
							url: 'php/?case=TrashProduct',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Cache-Control' : 'no-cache'
							},
							data: 'productId='+productId
						}).then(function(res){
							if(res.data.code == 0){
								$scope.productList = res.data.text;
							}else{
								$scope.productList = [];
								$scope.productListError = res.data.text;
							}
						}, function(error){
							$ngConfirm({
								boxWidth: '20%',
								useBootstrap: false,
								title: false,
								content: error,
								type: 'red',
								typeAnimated: true,
								buttons: {
									close: {
										btnClass: 'btn-red',
										keys: ['enter', 'esc'],
									}
								}
							});
						});
					}
				},
				no: {
					keys: ['esc']
				},
			}
		});
	};
});

app.controller('SupportCtrl', function($scope, $http, $ngConfirm){
	$scope.options = {
		height: '200px',
		toolbar: [
			{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
			{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar' ] },
			{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'tools', items: [ 'Maximize' ] }
		],
	};
	// Get all support request
	$scope.helpListError = '';
	$scope.helpList = [];
	$http({
		url: 'php/?case=GetAllSupports',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.helpList = res.data.text;
		}else{
			$scope.helpListError = res.data.text;
		}
	}, function(error){
		$scope.helpListError = error;
	});

	$scope.OpenSupportModal = function(supportId){
		$http({
			url: 'php/?case=GetSupportByID',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			},
			data: 'SupportID='+supportId
		}).then(function(res){
			if(res.data.code == 0){
				$scope.support.concern   = res.data.text.Support_Concern;
				$scope.support.reply     = unescape('<p>Hi '+res.data.text.user.User_FullName+',</p>');
				$scope.support.email     = res.data.text.user.User_Email;
				$scope.support.name      = res.data.text.user.User_FullName;
				$('#supportModal').modal('show');
			}else{
				$scope.helpListError = res.data.text;
			}
		}, function(error){
			$scope.helpListError = error;
		});
	};

	$scope.TrashSupport = function(supportId){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: 'Are you sure to delete this ticket?',
			type: 'red',
			typeAnimated: true,
			buttons: {
				yes: {
					btnClass: 'btn-red',
					keys: ['enter'],
					action: function(scope, button){
						$http({
							url: 'php/?case=TrashSupport',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Cache-Control' : 'no-cache'
							},
							data: 'supportId='+supportId
						}).then(function(res){
							if(res.data.code == 0){
								$scope.helpList = res.data.text;
							}else{
								$scope.helpList = [];
								$scope.helpListError = res.data.text;
							}
						}, function(error){
							$ngConfirm({
								boxWidth: '20%',
								useBootstrap: false,
								title: false,
								content: error,
								type: 'red',
								typeAnimated: true,
								buttons: {
									close: {
										btnClass: 'btn-red',
										keys: ['enter', 'esc'],
									}
								}
							});
						});
					}
				},
				no: {
					keys: ['esc']
				},
			}
		});
	};

	$scope.SendSupportReply = function(support){
		if(support.reply){
			$http({
				url: 'php/?case=ReplySupport',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Cache-Control' : 'no-cache'
				},
				data: 'email='+support.email+'&name='+support.name+'&reply='+escape(support.reply),
			}).then(function(res){
				if(res.data.code == 0){
					$scope.helpSuccess = 'Reply has been successfully sent.';
					$scope.helpError = '';
				}else{
					$scope.helpError = res.data.text;
					$scope.helpSuccess = '';
				}
			}, function(error){
				$scope.helpError = error;
				$scope.helpSuccess = '';
			});
		}else{
			$scope.helpError = 'Please enter your message.';
			$scope.helpSuccess = '';
		}
	};

	// Get all consultation request
	$scope.consultListError = '';
	$scope.consultList = [];
	$http({
		url: 'php/?case=GetAllConsultations',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.consultList = res.data.text;
		}else{
			$scope.consultListError = res.data.text;
		}
	}, function(error){
		$scope.consultListError = error;
	});

	$scope.OpenConsultModal = function(consultId){
		$http({
			url: 'php/?case=GetConsultByID',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			},
			data: 'ConsultID='+consultId
		}).then(function(res){
			if(res.data.code == 0){
				$scope.consult.subject   = res.data.text.Con_Subject;
				$scope.consult.query   	 = res.data.text.Con_Query;
				$scope.consult.reply   	 = unescape('<p>Hi '+res.data.text.user.User_FullName+',</p>');
				$scope.consult.email     = res.data.text.user.User_Email;
				$scope.consult.name      = res.data.text.user.User_FullName;
				$('#consultModal').modal('show');
			}else{
				$scope.consultListError = res.data.text;
			}
		}, function(error){
			$scope.consultListError = error;
		});
	};

	$scope.TrashConsult = function(consultId){
		$ngConfirm({
			boxWidth: '20%',
			useBootstrap: false,
			title: false,
			content: 'Are you sure to delete this ticket?',
			type: 'red',
			typeAnimated: true,
			buttons: {
				yes: {
					btnClass: 'btn-red',
					keys: ['enter'],
					action: function(scope, button){
						$http({
							url: 'php/?case=TrashConsult',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Cache-Control' : 'no-cache'
							},
							data: 'consultId='+consultId
						}).then(function(res){
							if(res.data.code == 0){
								$scope.consultList = res.data.text;
							}else{
								$scope.consultList = [];
								$scope.consultListError = res.data.text;
							}
						}, function(error){
							$ngConfirm({
								boxWidth: '20%',
								useBootstrap: false,
								title: false,
								content: error,
								type: 'red',
								typeAnimated: true,
								buttons: {
									close: {
										btnClass: 'btn-red',
										keys: ['enter', 'esc'],
									}
								}
							});
						});
					}
				},
				no: {
					keys: ['esc']
				},
			}
		});
	};

	$scope.SendConsultReply = function(consult){
		if(consult.reply){
			$http({
				url: 'php/?case=ReplySupport',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Cache-Control' : 'no-cache'
				},
				data: 'email='+consult.email+'&name='+consult.name+'&subject='+consult.subject+'&reply='+escape(consult.reply),
			}).then(function(res){
				if(res.data.code == 0){
					$scope.consultSuccess = 'Reply has been successfully sent.';
					$scope.consultError = '';
				}else{
					$scope.consultError = res.data.text;
					$scope.consultSuccess = '';
				}
			}, function(error){
				$scope.consultError = error;
				$scope.consultSuccess = '';
			});
		}else{
			$scope.consultError = 'Please enter your message.';
			$scope.consultSuccess = '';
		}
	};

	$scope.ClearForm = function(){
		$scope.support = {};
		$scope.helpError = '';
		$scope.helpSuccess = '';

		$scope.consult = {};
		$scope.consultError = '';
		$scope.consultSuccess = '';

		$scope.emailSuccess = '';
		$scope.emailError = '';
	};

	$scope.se = {
		page: 'SupportEmail',
		content: ''
	};
	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.se.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.se.content = unescape(res.data.text.Page_Content);
		}
	});

	$scope.ce = {
		page: 'ConsultationEmail',
		content: ''
	};
	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.ce.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.ce.content = unescape(res.data.text.Page_Content);
		}
	});

	$scope.cs = {
		page: 'ConsultationSubject',
		content: ''
	};
	$http({
		url: 'php/?case=GetPageContent',
		method: 'POST',
		data: 'page='+$scope.cs.page,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Cache-Control' : 'no-cache'
		}
	}).then(function(res){
		if(res.data.code == 0){
			$scope.cs.content = unescape(res.data.text.Page_Content);
		}
	});

	$scope.SavePageContent = function(content){
		$http({
			url: 'php/?case=SavePageContent',
			method: 'POST',
			data: 'page='+content.page+'&content='+escape(content.content),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			}
		}).then(function(res){
			if(res.data.code == 0){
				$scope.emailError = '';
				$scope.emailSuccess = res.data.text;
			}else{
				$scope.emailSuccess = '';
				$scope.emailError = res.data.text;
			}
		}, function(error){
			$scope.emailSuccess = '';
			$scope.emailError = error;
		});
	};
});
