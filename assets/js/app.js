'use strict';

var app = angular.module('SpeedDial', [
  'ngRoute',
  'angularjs-bootstrap-datetimepicker',
  'colorpicker',
  'ckeditor',
  'cp.ngConfirm'
]);

app.run(function($rootScope, $location, loginService){
  var authPermission = ['/users', '/media', '/events', '/products', 'faq-support'];
  var noAuthPermission = ['/'];
  $rootScope.$on('$routeChangeStart', function(){
    if(authPermission.indexOf($location.path()) != -1 && !loginService.isLogged()){
      $location.path('/');
    }
    if(noAuthPermission.indexOf($location.path()) != -1 && loginService.isLogged()){
      $location.path('/users');
    }
  });
});

app.config(function($routeProvider){
  $routeProvider
  .when('/', {
    templateUrl: 'partials/login.html',
    controller: 'LoginCtrl'
  })
  .when('/users', {
    templateUrl: 'partials/users.html',
    controller: 'UsersCtrl'
  })
  .when('/content', {
    templateUrl: 'partials/content.html',
    controller: 'ContentCtrl'
  })
  .when('/media', {
    templateUrl: 'partials/media.html',
    controller: 'MediaCtrl'
  })
  .when('/events', {
    templateUrl: 'partials/events.html',
    controller: 'EventsCtrl'
  })
  .when('/products', {
    templateUrl: 'partials/products.html',
    controller: 'ProductsCtrl'
  })
  .when('/support', {
    templateUrl: 'partials/support.html',
    controller: 'SupportCtrl'
  })
  .otherwise({
    redirectTo: '/'
  });
});
