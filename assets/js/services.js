'use strict';

app.factory('loginService', function($http, $location, $q, sessionService){
  return {
    login: function(signin, scope){
      var defer = $q.defer();
      $http.post('php/?case=LoginProcess', signin)
      .success(function(res){
        defer.resolve(res);
      })
      .error(function(err, status){
        defer.reject(err);
      });
      return defer.promise;
    },
    logout: function(){
      sessionService.destroy('admin');
      $location.path('/');
    },
    isLogged: function(){
      if(sessionService.get('admin')){
        return true;
      }
    }
  }
});

app.factory('sessionService', function($http){
  return {
    set: function(key, value){
      return sessionStorage.setItem(key, value);
    },
    get: function(key){
      return sessionStorage.getItem(key);
    },
    destroy: function(key){
      return sessionStorage.removeItem(key);
    }
  }
});
