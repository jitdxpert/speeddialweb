<?php

error_reporting(0);
ini_set('display_errors', 'Off');

include(dirname(dirname(__FILE__)) . '/php/config.php');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Cache-Control');
header('Cache-Control: no-cache');

$json = inputs();
$case = $json->case;
DB::table('logs')->insert(array(
  'content' => json_encode($json),
  'uri' => $case,
  'timestamp' => date('Y-m-d H:i:s'),
));

switch($case){
	case 'SocialSigninProcess':
		SocialSigninProcess();
		break;
	case 'SignupProcess':
		SignupProcess();
		break;
	case 'SigninProcess':
		SigninProcess();
		break;
	case 'CreateLogID':
		CreateLogID();
		break;
	case 'LogoutProcess':
		LogoutProcess();
		break;
	case 'ForgotProcess':
		ForgotProcess();
		break;
	case 'GetUserDetails':
		GetUserDetails();
		break;
	case 'UpdateDevice':
		UpdateDevice();
		break;

  case 'SpeedDialCalendar':
      SpeedDialCalendar();
      break;
	case 'GetSpeedDialContent':
		GetSpeedDialContent();
		break;
	case 'GetAboutUsContent':
		GetAboutUsContent();
		break;
	case 'GetButtonContent':
		GetButtonContent();
		break;
	case 'GetConsultationSubject':
		GetConsultationSubject();
		break;
	case 'GetNotificationTime':
		GetNotificationTime();
		break;
	case 'SaveLocalNotify':
		SaveLocalNotify();
		break;
	case 'GetLocalNotificationByUser':
		GetLocalNotificationByUser();
		break;

	case 'GetAllVideos':
		GetAllVideos();
		break;
	case 'GetAllAudios':
		GetAllAudios();
		break;

	case 'GetAllEvents':
		GetAllEvents();
		break;

	case 'SaveConsultation':
		SaveConsultation();
		break;

	case 'SaveSupport':
		SaveSupport();
		break;

	case 'SaveSpeedDial':
		SaveSpeedDial();
		break;
	case 'EmailSpeedDial':
		EmailSpeedDial();
		break;
	case 'GetAllSpeedDial':
		GetAllSpeedDial();
		break;
	case 'GetSpeedDial':
		GetSpeedDial();
		break;
	case 'SaveStatus':
		SaveStatus();
		break;
	case 'GetStats':
		GetStats();
		break;
	case 'GetOneMonthStats':
	    GetOneMonthStats();
	    break;

	case 'UploadProfileImage':
		UploadProfileImage();
		break;
	case 'UploadProfileImageNew':
	    UploadProfileImageNew();
	    break;
	case 'UpdateReminder':
		UpdateReminder();
		break;
	case 'UpdateReminderTime':
	    UpdateReminderTime();
	    break;
	case 'ChangePassword':
		ChangePassword();
		break;
	case 'SaveRegistrationId':
	    SaveRegistrationId();
	    break;

	default:
		echo '404! Page Not Found';
		break;
}

function SocialSigninProcess(){
	$res = array();

	$json = inputs();
	$user = DB::table('users')->where('User_Email', '=', $json->email)->first();
	if(count($user) == 0){
		$res['first'] = 1;
		$userArray = array(
			'User_SID' => $json->sId,
			'User_FullName' => $json->name,
			'User_Email' => $json->email,
			'User_ScreenName' => $json->screen,
			'User_Gender' => $json->sex,
			'User_Image' => $json->image,
			'User_From' => $json->media,
			'User_RegisterOn' => date('Y-m-d H:i:s'),
			'Reminder_Time' => '22:00',
		);
		$User_ID = DB::table('users')->insertGetId($userArray);

		$subject = 'Welcome to SpeedDial';
		$message = '<p>Hi ' . $json->name . '</p>';
		$message .= '<p>Thanks for signing up to keep in touch with SpeedDial.</p>';
		$message .= '<p>Cheers,<br />SpeedDial</p>';
		Send_Mail($subject, $message, $json->name, $json->email);

		$user = DB::table('users')->where('User_ID', '=', $User_ID)->first();
	}else{
		$res['first'] = 0;
		$User_ID = $user['User_ID'];
	}
	$res['code'] = 0;
	$res['text'] = $user;

	session_start();
	$_SESSION['User_ID'] = $User_ID;
	DB::table('users')->where('User_ID', '=', $User_ID)->update(array('User_FullName' => $json->name, 'User_Image' => $json->image, 'User_LastLogin' => date('Y-m-d H:i:s'), 'User_TzOffset' => isset($json->tzoffset) ? $json->tzoffset : null));
	$logArray = array(
		'User_ID' => $User_ID,
		'Login_Time' => date('Y-m-d H:i:s')
	);
	$res['LogID'] = DB::table('logins')->insertGetId($logArray);

	echo json_encode($res);
}

function SignupProcess(){
	$res = array();

	$json = inputs();
	if(!empty($json->name) && !empty($json->email) && !empty($json->password)){
		$user = DB::table('users')->where('User_Email', '=', $json->email)->first();
		if(count($user) == 0){
			$userArray = array(
				'User_FullName' => $json->name,
				'User_Email' => $json->email,
				'User_Password' => $json->password,
				'User_Image' => 'img/male.jpg',
				'User_RegisterOn' => date('Y-m-d H:i:s'),
				'Reminder_Time' => '22:00',
			);
			$User_ID = DB::table('users')->insertGetId($userArray);

			$subject = 'Welcome to SpeedDial';
			$message = '<p>Hi ' . $json->name . '</p>';
			$message .= '<p>Thanks for signing up to keep in touch with SpeedDial.</p>';
			$message .= '<p>Cheers,<br />SpeedDial</p>';
			Send_Mail($subject, $message, $json->name, $json->email);

			$user = DB::table('users')->where('User_ID', '=', $User_ID)->first();
			$res['code'] = 0;
			$res['text'] = $user;

			session_start();
			$_SESSION['User_ID'] = $User_ID;
			DB::table('users')->where('User_ID', '=', $User_ID)->update(array('User_LastLogin' => date('Y-m-d H:i:s'), 'User_TzOffset' => isset($json->tzoffset) ? $json->tzoffset : ''));
			$logArray = array(
				'User_ID' => $User_ID,
				'Login_Time' => date('Y-m-d H:i:s')
			);
			$res['LogID'] = DB::table('logins')->insertGetId($logArray);
		}else{
			$res['code'] = 1;
			$res['text'] = 'Email ID is already exist.';
		}
	}else{
		$res['code'] = 2;
		$res['text'] = 'Name, Email & Password Required';
	}

	echo json_encode($res);
}

function SigninProcess(){
	$res = array();

	$json = inputs();
	if(!empty($json->email) && !empty($json->password)){
		$user = DB::table('users')->where('User_Email', '=', $json->email)->where('User_Password', '=', $json->password)->first();
		if(count($user) > 0){
			session_start();
			$User_ID = $user['User_ID'];
			$_SESSION['User_ID'] = $User_ID;
			$res['code'] = 0;
			$res['text'] = $user;

			DB::table('users')->where('User_ID', '=', $User_ID)->update(array('User_LastLogin' => date('Y-m-d H:i:s')));
			$logArray = array(
				'User_ID' => $User_ID,
				'Login_Time' => date('Y-m-d H:i:s')
			);
			$res['LogID'] = DB::table('logins')->insertGetId($logArray);
		}else{
			$res['code'] = 1;
			$res['text'] = 'Wrong Email Or Password';
		}
	}else{
		$res['code'] = 2;
		$res['text'] = 'Email & Password Required';
	}

	echo json_encode($res);
}

function CreateLogID(){
	$res = array();

	$json = inputs();
	if(isset($json->userid)){
	    $userId = $json->userid;
	}else{
	    $userId = $json;
	}
	if(isset($json->tzoffset)){
	    $tzOffset = $json->tzoffset;
	}else{
	    $tzOffset = '';
	}

	DB::table('users')->where('User_ID', '=', $userId)->update(array('User_LastLogin' => date('Y-m-d H:i:s'), 'User_TzOffset' => $tzOffset));
	$logArray = array(
		'User_ID' => $userId,
		'Login_Time' => date('Y-m-d H:i:s')
	);
	$res['LogID'] = DB::table('logins')->insertGetId($logArray);
	$res['code'] = 0;

	echo json_encode($res);
}

function LogoutProcess(){
	$res = array();

	$json = inputs();
	DB::table('logins')->where('Login_ID', '=', $json->logId)->where('User_ID', '=', $json->userid)->update(array('Logout_Time' => date('Y-m-d H:i:s')));
    // DB::table('users')->where('User_ID', '=', $json->userid)->update(array('Registration_ID' => null));

	DB::table('logins')->where('Login_Time', '=', '0000-00-00 00:00:00')->where('Logout_Time', '=', '0000-00-00 00:00:00')->where('User_ID', '=', $json->userid)->delete();

	session_start();
	unset($_SESSION['User_ID']);
	$res['code'] = 0;
	$res['text'] = 'Successfully Logged Out.';

	echo json_encode($res);
}

function ForgotProcess(){
	$res = array();

	$json = inputs();
	if(!empty($json->email)){
		$user = DB::table('users')->where('User_Email', '=', $json->email)->first();
		if(count($user) > 0){
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
			$password = substr(str_shuffle($chars), 0, 8);
			DB::table('users')->where('User_Email', '=', $json->email)->update(array('User_Password' => $password));

			$subject = 'Password Reset SpeedDial';
			$message = '<p>Hi ' . $user['User_FullName'] . '</p>';
			$message .= '<p>We have received a request to reset password from you.<br />Here is your new password: ' . $password . '</p>';
			$message .= '<p>Cheers,<br />SpeedDial</p>';
			Send_Mail($subject, $message, $user['User_FullName'], $json->email);

			$res['code'] = 0;
			$res['text'] = 'New Password Sent To Your Email';
		}else{
			$res['code'] = 1;
			$res['text'] = 'Wrong Email';
		}
	}else{
		$res['code'] = 2;
		$res['text'] = 'Email Required';
	}

	echo json_encode($res);
}

function GetUserDetails(){
	$res = array();

	$User_ID = inputs();
	if($User_ID){
		$user = DB::table('users')->where('User_ID', '=', $User_ID)->first();
		if(count($user) > 0){
			$res['code'] = 0;
			$res['text'] = $user;
		}else{
			$res['code'] = 1;
			$res['text'] = 'No User Found';
		}
	}else{
		$res['code'] = 2;
		$res['text'] = 'Session Expire Login Again';
	}

	echo json_encode($res);
}

function UpdateDevice(){
	$res = array();

	$json = inputs();
	if(!empty($json->device)){
		DB::table('users')->where('User_ID', '=', $json->userid)->update(array('User_Device' => $json->device));
		$res['code'] = 0;
	}else{
		$res['code'] = 1;
	}

	echo json_encode($res);
}

function SpeedDialCalendar(){
    $res = false;
    $prev_year_month = false;
    $next_year_month = false;

    $json = inputs();
    $year_month = isset($json->year_month) ? $json->year_month : '';

    // $user = DB::table('users')->where('User_ID', $json->userid)->first();
    // setUserTimeZone($user['User_TzOffset']);

    if(!$year_month){
      $year_month = date('Y-m');
    }

    if($year_month != date('Y-m', strtotime('-2 month'))){
      $prev_year_month = date('Y-m', strtotime($year_month.' -1 month'));
    }

    if($year_month != date('Y-m', strtotime('+2 month'))){
      $next_year_month = date('Y-m', strtotime($year_month.' +1 month'));
    }
    $year_month_text = date('F,Y', strtotime($year_month));

    $year_month_array = explode('-', $year_month);
    $year  = $year_month_array[0];
    $month = $year_month_array[1];

    $offset = 1 - date('w', mktime(12, 0, 0, $month, 1, $year));
    if($offset == '-5' && $month != '2'):
      $cells = 42;
    elseif($offset == '1' && $year%4 != 0 && $month == '2'):
      $cells = 28;
    endif;


    $count = 42;
    $class = '';
    for($day=0; $day<$count; $day++):
      $time = mktime(12, 0, 0, $month, $day + $offset, $year);
      $week = date('w', $time);
      $index = (Int)$day/7;

      $colorIndex = rand(0,5);

      if(date('m', $time)!=$month):
        $calendar[$index][$week] = [];
      else:

        $date = date('Y-m-d', $time);
        $speeddial = DB::table('speeddials')->where('User_ID', '=', $json->userid)->where('Journal_Date', '=', $date)->get();

        $calendar[$index][$week] = [
          'hasItem' => count($speeddial),
          'text' => $day + $offset,
          'date' => $date,
          'today' => $date == date('Y-m-d') ? true : false,
          'format' => date('jS F, Y', $time),
          'selected' => $date == date('Y-m-d') ? true : false,
        ];
      endif;
    endfor;


    $res = [
      'code' => 0,
      'today' => [
         'text' => date('d'),
         'date' => date('Y-m-d'),
         'today' => true,
         'format' => date('jS F, Y'),
         'selected' => true,
      ],
      'calendar' => $calendar,
      'prev_year_month' => $prev_year_month,
      'next_year_month' => $next_year_month,
      'year_month_text' => $year_month_text
    ];

    echo json_encode($res);
}

function GetSpeedDialContent(){
	$res = array();

	$content = DB::table('contents')->where('Page_Name', '=', 'SpeedDial')->first();
	$res['code'] = 0;
	$res['text'] = $content;

	echo json_encode($res);
}

function GetAboutUsContent(){
	$res = array();

	$content = DB::table('contents')->where('Page_Name', '=', 'AboutUs')->first();
	$res['code'] = 0;
	$res['text'] = $content;

	echo json_encode($res);
}

function GetButtonContent(){
	$res = array();

	$content = DB::table('contents')->where('Page_Name', '=', 'FreeButton')->first();
	$res['text']['freeBtn'] = $content;

	$content = DB::table('contents')->where('Page_Name', '=', 'BuyButton')->first();
	$res['text']['buyBtn'] = $content;

	$content = DB::table('contents')->where('Page_Name', '=', 'ResourceButtonColor')->first();
	$res['text']['buttonColor'] = $content;

	$res['code'] = 0;

	echo json_encode($res);
}

function GetConsultationSubject(){
	$res = array();

	$content = DB::table('contents')->where('Page_Name', '=', 'ConsultationSubject')->first();
	$res['code'] = 0;
	$res['text'] = $content;

	echo json_encode($res);
}

function GetNotificationTime(){
	$res = array();

	$content = DB::table('contents')->where('Page_Name', '=', 'NotificationTime')->first();
	$res['code'] = 0;
	$res['text'] = $content;

	echo json_encode($res);
}

function SaveLocalNotify(){
	$res = array();

	$json = inputs();
	$notiArray = array(
		'User_ID' => $json->userID,
		'Local_ID' => $json->localID,
		'Noti_Date' => $json->notiDate,
		'CreateOn' => date('Y-m-d H:i:s')
	);
	$check = DB::table('notifications')
	    ->where('User_ID', '=', $json->userID)
	    ->where('Noti_Date', '=', $json->notiDate)
	    ->first();
	if(!$check){
	    DB::table('notifications')->insert($notiArray);
	}else{
	    DB::table('notifications')
	    ->where('User_ID', '=', $json->userID)
	    ->where('Noti_Date', '=', $json->notiDate)
	    ->update($notiArray);
	}
	$res['code'] = 0;
	$res['text'] = 'Local Notification Saved';

	echo json_encode($res);
}

function GetLocalNotificationByUser(){
	$res = array();

	$json = inputs();

    // $user = DB::table('users')->where('User_ID', $json->userID)->first();
    // setUserTimeZone($user['User_TzOffset']);

	$notifications = DB::table('notifications')->where('User_ID', '=', $json->userID)->where('Noti_Date', '>=', date('Y-m-d'))->get();
	if(count($notifications) > 0){
		$res['code'] = 0;
		$res['notifications'] = $notifications;
	}else{
		$res['code'] = 1;
	}

	$res['today'] = date('Y-m-d');

	echo json_encode($res);
}

function GetAllVideos(){
	$res = array();

	$videos = DB::table('videos')->orderBy('Video_ID', 'desc')->get();
	if(count($videos) > 0){
		$res['code'] = 0;
		$res['text'] = $videos;
	}else{
		$res['code'] = 1;
		$res['text'] = 'No Videos Available';
	}

	echo json_encode($res);
}

function GetAllAudios(){
	$res = array();

	$audios = DB::table('audios')->orderBy('Audio_ID', 'desc')->get();
	if(count($audios) > 0){
		$res['code'] = 0;
		$res['text'] = $audios;
	}else{
		$res['code'] = 1;
		$res['text'] = 'No Audios Available';
	}

	echo json_encode($res);
}

function GetAllEvents(){
	$res = array();

	$i = 0;
	$events = DB::table('events')->get();
	if(count($events) > 0){
		foreach($events as $event){
		    $event['From'] = 'event';
			$res['text'][$i] = $event;
			$i++;
		}
	}

	$products = DB::table('products')->get();
	if(count($products) > 0){
		foreach($products as $product){
		    $product['From'] = 'product';
			$res['text'][$i] = $product;
			$i++;
		}
	}

	function sortByOrder($a, $b){
	    return $a['CreateOn'] < $b['CreateOn'];
	}
	if(is_array($res['text'])){
		usort($res['text'], 'sortByOrder');
	}

    $res['code'] = 0;
	if(count($events) == 0 && count($products) == 0){
	    $res['code'] = 1;
		$res['text'] = 'No Events/Poducts Available';
	}

	echo json_encode($res);
}


function SaveConsultation(){
	$res = array();

	$json = inputs();
	if(!empty($json->subject) && !empty($json->query) && !empty($json->state) && !empty($json->mobile) && !empty($json->skype)){
		$userArray = array(
			'User_State' => $json->state,
			'User_Mobile' => $json->mobile,
			'User_Skype' => $json->skype
		);
		DB::table('users')->where('User_ID', '=', $json->userid)->update($userArray);

		$consultArray = array(
			'User_ID' => $json->userid,
			'Con_Subject' => $json->subject,
			'Con_Query' => $json->query,
			'Con_CreateOn' => date('Y-m-d H:i:s')
		);
		DB::table('consultations')->insert($consultArray);

		$user = DB::table('users')->where('User_ID', '=', $json->userid)->first();
		if(count($user) > 0){
		  $subject = $json->subject;

			$content = DB::table('contents')->select('Page_Content')->where('Page_Name', '=', 'ConsultEmailBody')->first();
			$message = '<p>Hi ' . $user['User_FullName'] . '</p>';
			$message .= $content['Page_Content'];
			Send_Mail($subject, $message, $user['User_FullName'], $user['User_Email']);

			$content = DB::table('contents')->select('Page_Content')->where('Page_Name', '=', 'ConsultationEmail')->first();
			$message = '<p>Name: ' . $user['User_FullName'] . '</p>';
			$message .= '<p>Email: ' . $user['User_Email'] . '</p>';
			$message .= '<p>Mobile: ' . $user['User_Mobile'] . '</p>';
			$message .= '<p>Skype: ' . $user['User_Skype'] . '</p>';
			$message .= '<p>Country/State: ' . $user['User_State'] . '</p>';
			$message .= '<p>WHAT IS YOUR BIGGEST ISSUE WITH YOUR BIZ NOW?<br />';
			$message .= $json->query . '</p>';
			$message .= '<p>Cheers,<br />SpeedDial</p>';
			Send_Mail($subject, $message, 'SpeedDial', $content['Page_Content'], $user['User_FullName'], $user['User_Email']);
		}
		$res['code'] = 0;
		$res['text'] = 'Your Request Has Been Sent & We Will Get Back To You In 48 Hours';
	}else{
		$res['code'] = 1;
		$res['text'] = 'All Fields Required';
	}

	echo json_encode($res);
}

function SaveSupport(){
	$res = array();

	$json = inputs();
	if(!empty($json->concern)){
		$supportArray = array(
			'User_ID' => $json->userid,
			'Support_Concern' => $json->concern,
			'Support_CreateOn' => date('Y-m-d H:i:s')
		);
		DB::table('supports')->insert($supportArray);
		$user = DB::table('users')->where('User_ID', '=', $json->userid)->first();
		if(count($user) > 0){
			$content = DB::table('contents')->select('Page_Content')->where('Page_Name', '=', 'SupportEmailBody')->first();
			$subject = 'SpeedDial Support Ticket';
			$message = '<p>Hi ' . $user['User_FullName'] . '</p>';
			$message .= $content['Page_Content'];
			Send_Mail($subject, $message, $user['User_FullName'], $user['User_Email']);

			$content = DB::table('contents')->select('Page_Content')->where('Page_Name', '=', 'SupportEmail')->first();
			$subject = 'SpeedDial Support Ticket';
			$message = '<p>' . $json->concern . '</p>';
			$message .= '<p>Cheers,<br />SpeedDial</p>';
			Send_Mail($subject, $message, 'SpeedDial', $content['Page_Content'], $user['User_FullName'], $user['User_Email']);
		}
		$res['code'] = 0;
		$res['text'] = 'Your Support Query Sent';
	}else{
		$res['code'] = 1;
		$res['text'] = 'Field Required';
	}

	echo json_encode($res);
}

function SaveSpeedDial(){

	$res = array();

	$json = inputs();

	$journalArray = array(
		'Journal_Date' => $json->date,
		'Answer_1' => isset($json->question1) ? $json->question1 : null,
		'Answer_2' => isset($json->question2) ? $json->question2 : null,
		'Answer_3' => isset($json->question3) ? $json->question3 : null,
		'Answer_4' => isset($json->question4) ? $json->question4 : null,
		'Answer_5' => isset($json->question5) ? $json->question5 : null,
		'Answer_6' => isset($json->question6) ? $json->question6 : null,
	);

	$journal = 	DB::table('speeddials')->where('User_ID', '=', $json->userid)->where('Journal_Date', '=', $json->date)->first();
	if(!$journal){
		$journalArray['User_ID'] = $json->userid;
		$journalArray['CreateOn'] = date('Y-m-d H:i:s');
		$JournalId = DB::table('speeddials')->insertGetId($journalArray);
	}else{
		$JournalId = $journal['Journal_ID'];
		DB::table('speeddials')->where('Journal_ID', '=', $JournalId)->update($journalArray);
	}

	$skip = 6;
	$count = DB::table('rocks')->where('User_ID', '=', $json->userid)->where('Journal_ID', '=', $JournalId)->where('Rock_Date', '=', $json->date)->orderBy('Rock_ID', 'asc')->count();
	if($count > $skip){
    	$take = $count - $skip;
    	$rocks = DB::table('rocks')->where('User_ID', '=', $json->userid)->where('Journal_ID', '=', $JournalId)->where('Rock_Date', '=', $json->date)->orderBy('Rock_ID', 'asc')->skip($skip)->take($take)->get();
    	foreach($rocks as $rock){
    	    DB::table('rocks')->where('Rock_ID', '=', $rock['Rock_ID'])->delete();
    	}
	}

	$rocks = $json->rocks;


	$rockId1 = isset($rocks->rockId1) ? $rocks->rockId1 : 0;
	$rock1 = isset($rocks->rock1) ? $rocks->rock1 : null;
	storeRocks($rockId1, 1, $rock1, $json->date, $JournalId, $json->userid);

	$rockId2 = isset($rocks->rockId2) ? $rocks->rockId2 : 0;
	$rock2 = isset($rocks->rock2) ? $rocks->rock2 : null;
	storeRocks($rockId2, 2, $rock2, $json->date, $JournalId, $json->userid);

	$rockId3 = isset($rocks->rockId3) ? $rocks->rockId3 : 0;
	$rock3 = isset($rocks->rock3) ? $rocks->rock3 : null;
	storeRocks($rockId3, 3, $rock3, $json->date, $JournalId, $json->userid);

	$rockId4 = isset($rocks->rockId4) ? $rocks->rockId4 : 0;
	$rock4 = isset($rocks->rock4) ? $rocks->rock4 : null;
	storeRocks($rockId4, 4, $rock4, $json->date, $JournalId, $json->userid);

	$rockId5 = isset($rocks->rockId5) ? $rocks->rockId5 : 0;
	$rock5 = isset($rocks->rock5) ? $rocks->rock5 : null;
	storeRocks($rockId5, 5, $rock5, $json->date, $JournalId, $json->userid);

	$rockId6 = isset($rocks->rockId6) ? $rocks->rockId6 : 0;
	$rock6 = isset($rocks->rock6) ? $rocks->rock6 : null;
	storeRocks($rockId6, 6, $rock6, $json->date, $JournalId, $json->userid);

    $hasItem = false;
    if($rock1 || $rock2 || $rock3 || $rock4 || $rock5 || $rock6){
        $hasItem = true;
    }

    // $user = DB::table('users')->where('User_ID', $json->userid)->first();
	// setUserTimeZone($user['User_TzOffset']);

	$res['code'] = 0;
	$res['hasItem'] = $hasItem;
	$res['today'] = date('Y-m-d');
	$res['text'] = 'Journal Saved';

	echo json_encode($res);
}

function EmailSpeedDial(){
	$res = array();

	$json = inputs();
	$journalArray = array(
		'Journal_Date' => $json->date,
		'Answer_1' => isset($json->question1) ? $json->question1 : '',
		'Answer_2' => isset($json->question2) ? $json->question2 : '',
		'Answer_3' => isset($json->question3) ? $json->question3 : '',
		'Answer_4' => isset($json->question4) ? $json->question4 : '',
		'Answer_5' => isset($json->question5) ? $json->question5 : '',
		'Answer_6' => isset($json->question6) ? $json->question6 : ''
	);

	$rock1 = isset($json->rocks->rock1) ? $json->rocks->rock1 : '';
	$rock2 = isset($json->rocks->rock2) ? $json->rocks->rock2 : '';
	$rock3 = isset($json->rocks->rock3) ? $json->rocks->rock3 : '';
	$rock4 = isset($json->rocks->rock4) ? $json->rocks->rock4 : '';
	$rock5 = isset($json->rocks->rock5) ? $json->rocks->rock5 : '';
	$rock6 = isset($json->rocks->rock6) ? $json->rocks->rock6 : '';

	$toName = isset($json->name) ? $json->name : '';
	$toEmail = isset($json->email) ? $json->email : '';

    if($toName && $toEmail){
    	$string = '';
    	if($journalArray['Answer_1']){
    		$string .= '<div style="margin-bottom:20px;border-bottom:1px solid #ddd;padding-bottom:10px;">';
    			$string .= '<p><strong>&bull; Today, I appreciate:</strong></p>';
    			$string .= '<p>' . $journalArray['Answer_1'] . '</p>';
    		$string .= '</div>';
    	}
    	if($journalArray['Answer_2']){
    		$string .= '<div style="margin-bottom:20px;border-bottom:1px solid #ddd;padding-bottom:10px;">';
    			$string .= '<p><strong>&bull; Today my dominant feeling will be:</strong></p>';
    			$string .= '<p>' . $journalArray['Answer_2'] . '</p>';
    		$string .= '</div>';
    	}

    	if($rock1 || $rock2 || $rock3 || $rock4 || $rock5 || $rock6){
    		$string .= '<div style="margin-bottom:20px;border-bottom:1px solid #ddd;padding-bottom:10px;">';
    			$string .= '<p><strong>&bull; My 6 Inspired Actions for today are</strong></p>';
    			if($rock1){
    				$string .= '<p>1. ' . $rock1 . '</p>';
    			}
    			if($rock2){
    				$string .= '<p>2. ' . $rock2 . '</p>';
    			}
    			if($rock3){
    				$string .= '<p>3. ' . $rock3 . '</p>';
    			}
    			if($rock4){
    				$string .= '<p>4. ' . $rock4 . '</p>';
    			}
    			if($rock5){
    				$string .= '<p>5. ' . $rock5 . '</p>';
    			}
    			if($rock6){
    				$string .= '<p>6. ' . $rock6 . '</p>';
    			}
    		$string .= '</div>';
    	}

    	if($journalArray['Answer_3']){
    		$string .= '<div style="margin-bottom:20px;border-bottom:1px solid #ddd;padding-bottom:10px;">';
    			$string .= '<p><strong>&bull; Everything else I desire is being delegated to the Universe. Thank you Universe, for handling the following for me!</strong></p>';
    			$string .= '<p>' . $journalArray['Answer_3'] . '</p>';
    		$string .= '</div>';
    	}
    	if($journalArray['Answer_4']){
    		$string .= '<div style="margin-bottom:20px;border-bottom:1px solid #ddd;padding-bottom:10px;">';
    			$string .= '<p><strong>&bull; My Wild Hair Intention. Wouldn\'t it be awesome today if:</strong></p>';
    			$string .= '<p>' . $journalArray['Answer_4'] . '</p>';
    		$string .= '</div>';
    	}
    	if($journalArray['Answer_5']){
    		$string .= '<div style="margin-bottom:20px;border-bottom:1px solid #ddd;padding-bottom:10px;">';
    			$string .= '<p><strong>&bull; One of my desires has manifested in the most perfect way. It unfolded like this...</strong></p>';
    			$string .= '<p>' . $journalArray['Answer_5'] . '</p>';
    		$string .= '</div>';
    	}
    	if($journalArray['Answer_6']){
    		$string .= '<div style="margin-bottom:20px;border-bottom:1px solid #ddd;padding-bottom:10px;">';
    			$string .= '<strong>&bull; The most important thing for me to focus on today is...</strong></p>';
    			$string .= '<p>' . $journalArray['Answer_6'] . '</p>';
    		$string .= '</div>';
    	}
    	$string .= '<br /><br /><p>Cheers,<br />SpeedDial</p>';

    	$subject  = 'SpeedDial Today\'s Journal';
    	$message  = $string;
    	Send_Mail($subject, $message, $toName, $toEmail);
    	$res['code'] = 0;
    	$res['text'] = 'Journal Sent';
    }else{
        $res['code'] = 1;
    	$res['text'] = 'Name & Email Required';
    }

	echo json_encode($res);
}

function GetAllSpeedDial(){
	$res = array();

	$User_ID = inputs();
	$speeddials = DB::table('speeddials')->where('User_ID', '=', $User_ID)->get();
	if(count($speeddials) > 0){
		$res['code'] = 0;
		$res['text']['journal'] = $speeddials;
	}else{
		$res['code'] = 1;
		$res['text'] = 'No journal found.';
	}

	echo json_encode($res);
}

function GetSpeedDial(){
	$res = array();

	$json = inputs();
	$speeddial = DB::table('speeddials')->where('User_ID', '=', $json->userid)->where('Journal_Date', '=', $json->date)->first();
	if(count($speeddial) > 0){
		$Journal_ID = $speeddial['Journal_ID'];
		$res['code'] = 0;
		$res['text']['journal'] = $speeddial;
		$rocks = DB::table('rocks')->where('User_ID', '=', $json->userid)->where('Journal_ID', '=', $Journal_ID)->where('Rock_Date', '=', $json->date)->orderBy('Rock_ID', 'asc')->take(6)->get();
		if(count($rocks) > 0){
			$i = 1;
			foreach($rocks as $rock){
				$res['text']['rockIds']['rockId' . $i] = $rock;
				$res['text']['rockAns']['rock' . $i] = $rock;
				$i++;
			}
		}
	}else{
		$res['code'] = 1;
		$res['text'] = 'No Journal Found';
	}

	echo json_encode($res);
}

function SaveStatus(){
	$res = array();

    $c = 0;
	$json = inputs();
	if(isset($json->check)){
		foreach($json->check as $key => $val){
			if($val){
			    $c++;
				DB::table('rocks')->where('Rock_ID', '=', $key)->where('User_ID', '=', $json->userid)->where('Rock_Date', '=', $json->date)->update(array('Rock_Status' => 'complete'));
			}else{
				DB::table('rocks')->where('Rock_ID', '=', $key)->where('User_ID', '=', $json->userid)->where('Rock_Date', '=', $json->date)->update(array('Rock_Status' => 'pending'));
			}
		}
	}

	$res['text'] = $c.' Inspired Actions Accomplished';

	echo json_encode($res);
}

function GetStats(){
	$res = array();

    $res['code'] = 0;
	$res['TotalRocks'] = 0;
	$res['PendingRocks'] = 0;
	$res['CompleteRocks'] = 0;

	$json = inputs();
	$rocks = DB::table('rocks')->where('User_ID', '=', $json->userid)->where('Rock_Date', '=', $json->date)->where('Answer', '!=', '')->get();
	if(count($rocks) > 0){
		$res['TotalRocks'] = count($rocks);
		foreach($rocks as $rock){
			if($rock['Rock_Status'] == 'pending'){
				$res['PendingRocks'] = $res['PendingRocks'] + 1;
			}else{
				$res['CompleteRocks'] = $res['CompleteRocks'] + 1;
			}
		}
	}else{
		$res['code'] = 1;
	}

	echo json_encode($res);
}


function UploadProfileImage(){
	$allowed =  array('png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'bmp', 'BMP');
	$upload_dir = BASE_DIR . "/uploads/profile/";

 	$UserID = $_REQUEST['UserID'];
 	$filename = $_FILES['file']['name'];
	$image_path = $upload_dir . basename($filename);
	$ext = pathinfo($filename, PATHINFO_EXTENSION);
	if(in_array($ext, $allowed)){
		if(move_uploaded_file($_FILES['file']['tmp_name'], $image_path)){
			$image_url = BASE_URL . 'uploads/profile/' . basename($filename);
			DB::table('users')->where('User_ID', '=', $UserID)->update(array('User_Image' => $image_url));
			echo 'Profile Image Updated';
		}else{
		    echo "Error Uploading Image, Try Again";
		}
	}else{
		echo 'Wrong Image Format';
	}
}

function UploadProfileImageNew(){
    $res = array();

	$allowed =  array('png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'bmp', 'BMP');
	$upload_dir = BASE_DIR . "/uploads/profile/";

 	$UserID = $_REQUEST['UserID'];
 	$filename = $_FILES['file']['name'];
	$image_path = $upload_dir . basename($filename);
	$ext = pathinfo($filename, PATHINFO_EXTENSION);
	if(in_array($ext, $allowed)){
		if(move_uploaded_file($_FILES['file']['tmp_name'], $image_path)){
			$image_url = BASE_URL . 'uploads/profile/' . basename($filename);
			DB::table('users')->where('User_ID', '=', $UserID)->update(array('User_Image' => $image_url));
			$res['code'] = 0;
			$res['image'] = $image_url;
			$res['text'] = 'Profile Image Updated';
		}else{
		    $res['code'] = 1;
		    $res['text'] = 'Error Uploading Image, Try Again';
		}
	}else{
	    $res['code'] = 2;
		$res['text'] = 'Wrong Image Format';
	}

	echo json_encode($res);
}

function UpdateReminder(){
  $res = array();

	$json = inputs();
	if($json->reminder == true){
		$reminder = '1';
		$res['text'] = 'Reminder On';
	}else{
		$reminder = '0';
		$res['text'] = 'Reminder Off';
	}
	DB::table('users')->where('User_ID', '=', $json->userid)->update(array('User_Reminder' => $reminder));

	echo json_encode($res);
}

function UpdateReminderTime(){
  $res = array();

	$json = inputs();
	DB::table('users')->where('User_ID', '=', $json->userid)->update(array('Reminder_Time' => $json->time));
	$res['text'] = 'Reminder Time Set';

	echo json_encode($res);
}

function ChangePassword(){
  $res = array();

	$json = inputs();
	if($json->userid){
	    $user = DB::table('users')->where('User_ID', '=', $json->userid)->first();
	    if($user['User_Password'] == $json->current){
	    		if($json->new == $json->confirm){
							DB::table('users')->where('User_ID', '=', $json->userid)->update(array('User_Password' => $json->new));
							$res['code'] = 0;
							$res['text'] = 'Password Changed';
					}else{
					    $res['code'] = 1;
							$res['text'] = 'Wrong Confirm Password';
					}
	    }else{
	        $res['code'] = 2;
					$res['text'] = 'Wrong Current Password';
			}
	}else{
	    $res['code'] = 3;
			$res['text'] = 'No User Found';
	}

	echo json_encode($res);
}

function SaveRegistrationId(){
    $json = inputs();
    $regId = isset($json->regId) ? $json->regId : '';
    $device = isset($json->device) ? $json->device : '';

    $user = DB::table('users')->where('Registration_ID', '=', $regId)->where('User_ID', '!=', $json->userid)->first();
    if($user){
        DB::table('users')->where('User_ID', '=', $user['User_ID'])->update(array('Registration_ID' => ''));
    }

	DB::table('users')->where('User_ID', '=', $json->userid)->update(array('Registration_ID' => $regId, 'User_Device' => $device));

	$res['code'] = 0;
	$res['text'] = 'Token Updated';

	echo json_encode($res);
}
